;;; scan.scm

(load (in-vicinity (getenv "topsrc") "tests/t-common.scm"))

(use-modules
 ((thud types) #:select (d-size blk-ports blk-nets blk-flops))
 ((thud state) #:select (session))
 ((thud svc0) #:select (add-only))
 ((thud com) #:select (assert)))

(define (check-blk blk num-ports num-nets num-flops)
  (assert blk)
  (assert (= num-ports (d-size (blk-ports blk))))
  (assert (= num-nets  (d-size (blk-nets  blk))))
  (assert (= num-flops (d-size (blk-flops blk)))))

;;;---------------------------------------------------------------------------
;;; Do it!

;; First, check a simple counter.
(do-test "scan simple"
  (add-only (examples/ "counters/counter.th"))
  (assert (= 1 (length (session list #:blocks))))
  (check-blk (session #:blocks 'counter) 1 0 1))

;; Next, a more complex one.
(do-test "scan complex"
  (add-only (examples/ "counters/complex-counter.th"))
  (assert (= 3 (length (session list #:blocks))))
  (check-blk (session #:blocks 'counter) 4 2 1)
  (check-blk (session #:blocks 'two-bit-no-good) 2 1 0))

(quit OK)

;;; scan.scm ends here
