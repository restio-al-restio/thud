;;; thgen.scm

(load (in-vicinity (getenv "topsrc") "tests/t-common.scm"))

(use-modules
 ((thud thgen) #:select (thgen
                         format-symbol
                         define-generator)))

(define (check-thgen! what expected)
  (equal? expected (thgen what '((seq-beg . 44) (seq-end . 45)))))

;;;---------------------------------------------------------------------------
;;; Do it.

(do-test-expect-fail "missing template and var-alist-proc"
  (define-generator 'no-way!))

(do-test-expect-fail "missing template"
  (define-generator 'no-way! #:var-alist-proc (lambda x '())))

(do-test-expect-fail "missing var-alist-proc"
  (define-generator 'no-way! #:template 'mux))

(do-test-expect-fail "bad var type"
  (define-generator 'no-way!
    #:vars '((i-pity-the-f 00 #:L!))
    #:template 'mux #:var-alist-proc (lambda x '())))

(do-test "thgen: simple mux"
  (check-thgen!
   'mux
   '(((blk mux44 (out 44 x) (in 44 a0) (in 44 a1) (in sel))
      (a. x (| (& (! sel) a0) (& sel a1))))
     ((blk mux45 (out 45 x) (in 45 a0) (in 45 a1) (in sel))
      (a. x (| (& (! sel) a0) (& sel a1)))))))

(define-generator 'mox
  #:template 'mux
  #:var-alist-proc
  (lambda (get)
    `((block-name . ,(format-symbol "mox~A" (get 'seed))))))

(do-test "thgen: mox"
  (check-thgen!
   'mox
   '(((blk mox44 (out 44 x) (in 44 a0) (in 44 a1) (in sel))
      (a. x (| (& (! sel) a0) (& sel a1))))
     ((blk mox45 (out 45 x) (in 45 a0) (in 45 a1) (in sel))
      (a. x (| (& (! sel) a0) (& sel a1)))))))

(define-generator 'mix
  #:template 'mox
  #:var-alist-proc
  (lambda (get)
    `((block-name . ,(format-symbol "mix~A" (get 'block-name)))
      (width . ,(* 2 (get 'seed))))))

(do-test "thgen: mix"
  (check-thgen!
   'mix
   '(((blk mixmox44 (out 88 x) (in 88 a0) (in 88 a1) (in sel))
      (a. x (| (& (! sel) a0) (& sel a1))))
     ((blk mixmox45 (out 90 x) (in 90 a0) (in 90 a1) (in sel))
      (a. x (| (& (! sel) a0) (& sel a1)))))))

(quit OK)

;;; thgen.scm ends here
