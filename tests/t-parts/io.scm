;;; io.scm

(load (in-vicinity (getenv "topsrc") "tests/t-common.scm"))

(use-modules
 ((thud state) #:select (session))
 ((thud svc0) #:select (add-only
                        prepare-to-simulate
                        dumpvars)))

(define (chk-num-dump-vars expected-value)
  (= expected-value ((session #:vcd-mangle) #:count)))

;;;---------------------------------------------------------------------------
;;; Do it!

;; First, check a simple counter.
;;
(do-test "[1] io counting vars using `(dumpvars)'"
  (add-only (examples/ "counters/counter.th"))
  (dumpvars)
  (prepare-to-simulate)
  (chk-num-dump-vars 1))

(do-test "[1] io counting vars using `(dumpvars '(a b))'"
  (add-only (examples/ "counters/counter.th"))
  (dumpvars '(/counter/val /counter/val-out))
  (prepare-to-simulate)
  (chk-num-dump-vars 1))

(do-test "[1] io counting vars using `(dumpvars 'a 'b)'"
  (add-only (examples/ "counters/counter.th"))
  (dumpvars '/counter/val '/counter/val-out)
  (prepare-to-simulate)
  (chk-num-dump-vars 1))

;; Now a more complex one.  Group tag is "[2]".
;;
(do-test "[2] io counting vars using `(dumpvars)'"
  (add-only (examples/ "counters/complex-counter.th"))
  (dumpvars)
  (prepare-to-simulate)
  (chk-num-dump-vars 7))

(do-test "[2] io counting vars using `(dumpvars '(a b))'"
  (add-only (examples/ "counters/complex-counter.th"))
  (dumpvars '(/sys/counter/val /sys/counter/val-out))
  (prepare-to-simulate)
  (chk-num-dump-vars 2))

(do-test "[2] io counting vars using `(dumpvars 'a 'b)'"
  (add-only (examples/ "counters/complex-counter.th"))
  (dumpvars '/sys/counter/val '/sys/counter/val-out)
  (prepare-to-simulate)
  (chk-num-dump-vars 2))

(quit OK)

;;; io.scm ends here
