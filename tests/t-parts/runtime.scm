;;; runtime.scm

(load (in-vicinity (getenv "topsrc") "tests/t-common.scm"))

(use-modules
 ((thud runtime) #:select (eval-in-the-thud-runtime-module))
 ((thud svc0) #:select (add-only reset-internals)))

(define runtime-bx   (eval-in-the-thud-runtime-module 'bx))
(define runtime-Bx   (eval-in-the-thud-runtime-module 'Bx))
(define runtime-bofh (eval-in-the-thud-runtime-module '*bofh*))

(define (check-bx sig bit expected)
  (do-test ("(bx ~A ~A) => ~A" sig bit expected)
    (= expected (runtime-bx sig bit))))

(define (check-Bx sig bit len expected)
  (do-test ("(Bx ~A ~A ~A) => ~A" sig bit len expected)
    (= expected (runtime-Bx sig bit len))))

(define (check-bofh expected . args)
  (do-test ("~S => ~A" (cons '*bofh* args) expected)
    (= expected (apply runtime-bofh args))))

;;;---------------------------------------------------------------------------
;;; Do it.

(reset-internals)
(add-only (examples/ "adder/adder.th"))

;; 23 is #b10111
(check-bx 23 0 1)                     ; bx
(check-bx 23 1 1)
(check-bx 23 2 1)
(check-bx 23 3 0)
(check-bx 23 4 1)
(check-Bx 23 0 1 #b1)                 ; Bx
(check-Bx 23 0 2 #b11)
(check-Bx 23 0 3 #b111)
(check-Bx 23 0 4 #b0111)
(check-Bx 23 0 5 #b10111)
(check-Bx 23 1 1 #b1)
(check-Bx 23 1 2 #b11)
(check-Bx 23 1 3 #b011)
(check-Bx 23 1 4 #b1011)
(check-Bx 23 2 1 #b1)
(check-Bx 23 2 2 #b01)
(check-Bx 23 2 3 #b101)
(check-Bx 23 3 1 #b0)
(check-Bx 23 3 2 #b10)
(check-Bx 23 4 1 #b1)

;; 2234234 is #b1000100001011101111010
(check-bx 2234234  0 0)               ; bx
(check-bx 2234234  1 1)
(check-bx 2234234  2 0)
(check-bx 2234234  3 1)
(check-bx 2234234  4 1)
(check-bx 2234234  5 1)
(check-bx 2234234  6 1)
(check-bx 2234234  7 0)
(check-bx 2234234  8 1)
(check-bx 2234234  9 1)
(check-bx 2234234 10 1)
(check-bx 2234234 11 0)
(check-bx 2234234 12 1)
(check-bx 2234234 13 0)
(check-bx 2234234 14 0)
(check-bx 2234234 15 0)
(check-bx 2234234 16 0)
(check-bx 2234234 17 1)
(check-bx 2234234 18 0)
(check-bx 2234234 19 0)
(check-bx 2234234 20 0)
(check-bx 2234234 21 1)
(check-Bx 2234234 19 2 #b00)          ; Bx
(check-Bx 2234234 5 10 #b0010111011)
(check-Bx 2234234 3 15 #b100001011101111)

;; first two args are EXPECTED and BIT-WIDTH
(check-bofh #b000 3 0 0)
(check-bofh #b000 3 0 0 0 1 0 2 0 3 0 4 0 5 0 6)
(check-bofh #b101 3 #b1 0 #b0 1 #b1 2)
(check-bofh #b101 3 #b1 2 #b0 1 #b1 0)
(check-bofh #xa45 12 #xa -4 8 #x4 -4 4 #x5 -4 0)
(check-bofh #x54a 12 #xa -4 0 #x4 -4 4 #x5 -4 8)
(check-bofh #x34a 12 #x4 -4 4 #xa -4 0 #x3 -4 8)
(check-bofh #xa43 12 #x4 -4 4 #xa -4 8 #x3 -4 0)

(quit OK)

;;; runtime.scm ends here
