;;; sched.scm

(load (in-vicinity (getenv "topsrc") "tests/t-common.scm"))

(use-modules
 ((thud types) #:select (make-dn dn-obj dn-back))
 ((thud state) #:select (session))
 ((thud com) #:select (dbg fln assert))
 ((thud svc0) #:select (reset-internals add-only)))

(define (sched-permute ls)
  (cond ((not (pair? ls))
         ls)
        ((= (length ls) 2)
         (list ls (reverse ls)))
        (else
         (apply append
                (map (lambda (x)
                       (map (lambda (y)
                              (append (list x) y))
                            (sched-permute (delete x ls))))
                     ls)))))

(define (display-ordering-list ord-ls)
  (for-each (lambda (x)
              (display " ")
              (display (dn-obj x)))
            ord-ls)
  (newline))

(define (check-no-forward-dep ord-ls)
  (let loop ((ls ord-ls))
    (or (not (pair? ls))
        (let ((following (map dn-obj (cdr ls))))
          (dbg DBGGEN
               (fln "for ~S ~S ..." (dn-obj (car ls)) (dn-back (car ls))))
          (for-each (lambda (dep)
                      (dbg DBGGEN
                           (fln "checking ~S against ~S" dep following))
                      (assert (not (memq dep following))))
                    (dn-back (car ls)))
          (loop (cdr ls))))))

(define (check-with-rtl file)
  (do-test ("sched file ~A" file)
    (reset-internals)
    (add-only (examples/ file))
    (check-no-forward-dep (session #:net-sched))))

;;;---------------------------------------------------------------------------
;;; Do it.

(check-with-rtl "adder/adder.th")
(check-with-rtl "counters/counter.th")
(check-with-rtl "counters/complex-counter.th")
(check-with-rtl "pipeline/pipeline.th")

(quit OK)

;;; sched.scm ends here
