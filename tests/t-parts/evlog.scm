;;; evlog.scm

(load (in-vicinity (getenv "topsrc") "tests/t-common.scm"))

(use-modules
 ((thud state) #:select (session))
 ((thud svc0) #:select (reset-internals)))

;;;---------------------------------------------------------------------------
;;; Do it.

(reset-internals)
(do-test "non-#f event log"
  (->bool (session #:log)))

(quit OK)

;;; evlog.scm ends here
