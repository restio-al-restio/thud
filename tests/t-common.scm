;;; t-common.scm

;; Copyright (C) 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

(define OK #t)

(define-macro (do-test blurb . body)
  `(let ((blurb ,(if (pair? blurb)
                     (apply simple-format #f blurb)
                     blurb))
         (val (begin ,@body)))
     (set! OK (and OK val))
     (or val (simple-format #t "FAIL: ~A\n" blurb))))

(define-macro (do-test-expect-fail blurb . body)
  `(let ((blurb ,(if (pair? blurb)
                     (apply simple-format #f blurb)
                     blurb))
         (val (not (false-if-exception (begin ,@body)))))
     (set! OK (and OK val))
     (or val (simple-format #t "FAIL: ~A\n" blurb))))

(define (topsrc/ rel)
  (in-vicinity (getenv "topsrc") rel))

(define (examples/ rel)
  (in-vicinity (topsrc/ "examples") rel))

(use-modules
 ((ice-9 rdelim) #:select (read-line))
 ((ice-9 popen) #:select (open-input-pipe close-pipe)))

(define (read-one-line command)
  (let* ((p (open-input-pipe command))
         (line (read-line p 'concat)))
    (close-pipe p)
    line))

;;; t-common.scm ends here
