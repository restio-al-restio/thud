;;; setup.scm

;; Copyright (C) 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

(debug-enable 'debug 'backtrace)

(use-modules (srfi srfi-13))
(use-modules (ice-9 testing-lib))

(define (topsrc/ relname)
  (in-vicinity (getenv "topsrc") relname))

(define (topbuild/ relname)
  (in-vicinity (getenv "topbuild") relname))

(define (scm-files directory)
  (let ((dirp (opendir directory)))
    (let loop ((acc '()))
      (let ((filename (readdir dirp)))
        (cond ((eof-object? filename)
               (closedir dirp)
               ;; rv
               (sort acc string<?))
              ((string-suffix? ".scm" filename)
               (loop (cons (in-vicinity directory filename)
                           acc)))
              (else
               (loop acc)))))))

(define (script-ok? filename)
  (pass-if (basename filename ".scm")
    (zero? (system (format #f "~A -s -x ~S 1>&~A 2>&~A"
                           (topbuild/ "thud")
                           filename
                           (fileno (current-output-port))
                           (fileno (current-error-port)))))))

;;; setup.scm ends here
