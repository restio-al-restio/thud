;;; 001.scm

(load (in-vicinity (getenv "topsrc") "tests/t-common.scm"))

(use-modules
 ((thud com) #:select (assert vn-val))
 ((thud svc0) #:select (add-only next)))

(define-macro (add-these-forms . forms)
  `(let ((tmpfile "001.TMP"))
     (with-output-to-file tmpfile
       (lambda ()
         (for-each (lambda (form)
                     (write form)
                     (newline))
                   ',forms)))
     (add-only tmpfile)
     (delete-file tmpfile)))

(define (next-10-and-check b e l c m)   ; support for bug 001
  (next 10)
  (fln "checking...")
  (assert (and (= (vn-val '/mutf-stim/mutf/bert)  b)
               (= (vn-val '/mutf-stim/mutf/ernie) e)
               (= (vn-val '/mutf-stim/mutf/larry) l)
               (= (vn-val '/mutf-stim/mutf/curly) c)
               (= (vn-val '/mutf-stim/mutf/moe)   m))))

;;;---------------------------------------------------------------------------
;;; Do it.

(do-test "flop evaluation does not handle mutual dependencies properly"
  (add-these-forms

   ;; mutf.th --- Mutually-dependent flops
   (blk mutf)

   (reg    bert)                        ; set 1: two flops
   (reg    ernie)

   (n. bert ernie)                      ; ping-pong
   (n. ernie bert)

   (reg    larry)                       ; set 2: three flops
   (reg    curly)
   (reg    moe)

   (n. larry curly)                     ; circular
   (n. curly moe)
   (n. moe larry)

   ;; mutf-stim.th --- Stimulus for mutf.th
   (blk mutf-stim)

   (w/ mutf mutf)

   ;; Ugh, this is really ugly.
   ;; See mutf-stim2.th for a better(?) approach.

   (reg 5 blah)
   (n. blah (begin
              (cond ((= 0 (cc))
                     (n! /mutf-stim/mutf/bert  0)
                     (n! /mutf-stim/mutf/ernie 1)
                     (n! /mutf-stim/mutf/larry 0)
                     (n! /mutf-stim/mutf/curly 0)
                     (n! /mutf-stim/mutf/moe   1))
                    ((= 13 (cc))
                     (n! /mutf-stim/mutf/bert  0)
                     (n! /mutf-stim/mutf/larry 1)
                     (n! /mutf-stim/mutf/curly 0)
                     (n! /mutf-stim/mutf/moe   1))
                    ((= 34 (cc))
                     (n! /mutf-stim/mutf/ernie 1)
                     (n! /mutf-stim/mutf/larry 0)
                     (n! /mutf-stim/mutf/curly 1)
                     (n! /mutf-stim/mutf/moe   0)))
              (cc)))

   (reg 5 blah2)
   (n. blah2 (begin
               (fln "~A\t~A ~A ~A ~A ~A" (cc)
                    /mutf-stim/mutf/bert
                    /mutf-stim/mutf/ernie
                    /mutf-stim/mutf/larry
                    /mutf-stim/mutf/curly
                    /mutf-stim/mutf/moe)
               (cc))))

  (next-10-and-check 0 1 1 0 0)
  (next-10-and-check 0 0 1 1 0)
  (next-10-and-check 0 0 1 0 1)
  (next-10-and-check 0 1 1 0 0))

(quit OK)

;;; 001.scm ends here
