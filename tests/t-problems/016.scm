;;; 016.scm

(load (in-vicinity (getenv "topsrc") "tests/t-common.scm"))

;;;---------------------------------------------------------------------------
;;; Do it.

(do-test "thud_4.html nested table is garbled."
  (eof-object? (read-one-line (string-append "egrep '^ +@' "
                                             (topsrc/ "doc/*.texi")))))

(quit OK)

;;; 016.scm ends here
