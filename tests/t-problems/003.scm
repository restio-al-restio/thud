;;; 003.scm

(load (in-vicinity (getenv "topsrc") "tests/t-common.scm"))

(use-modules
 ((thud svc0) #:select (reset-internals))
 ((thud state) #:select (session)))

;;;---------------------------------------------------------------------------
;;; Do it.

(do-test "no initialization to known state"
  (reset-internals)
  (and (null? (session list #:blocks))
       (null? (session list #:instances))
       (null? (session list #:aliases))
       (null? (session list #:vob))
       (= 1 (length (session #:log)))   ; reset adds entry
       (null? (session #:duty))
       (null? (session #:TODO))
       (null? (session list #:dangling))))

(quit OK)

;;; 003.scm ends here
