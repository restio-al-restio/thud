;;; 004.scm

(load (in-vicinity (getenv "topsrc") "tests/t-common.scm"))

(use-modules
 ((thud com) #:select (thud-execute-user-script)))

;;;---------------------------------------------------------------------------
;;; Do it.

(do-test "permute does not jam inputs"
  (let ((cur-dir (getcwd)))
    (chdir (examples/ "adder"))
    (thud-execute-user-script "demo-adder-noquit.scm")
    (chdir cur-dir))
  ;; FIXME: check something!
  #t)

(quit OK)

;;; 004.scm ends here
