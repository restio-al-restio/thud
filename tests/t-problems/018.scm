;;; 018.scm

(load (in-vicinity (getenv "topsrc") "tests/t-common.scm"))

(use-modules
 ((thud svc0) #:select (add reset-internals compile-to)))

;;;---------------------------------------------------------------------------
;;; Do it.

(do-test "using procedure `compile-to' causes an error"
  (reset-internals)
  (apply add (map examples/ '("adder/adder.th" "compilation/adder-stim.th")))
  (compile-to "018.out")
  (cond ((getenv "THUD_DEBUG")
         (system "sed /eform:/d 018.out > TMP")
         (rename-file "TMP" "018.out")
         (chmod "018.out" #o755)))
  (let ((ans (read-one-line
              "sed -n '/define/p;/defalias/p;/set!/p' 018.out | wc -l")))
    (or (getenv "THUD_DEBUG_KEEP_018") (delete-file "018.out"))
    (< 78 (with-input-from-string ans read))))

(quit OK)

;;; 018.scm ends here
