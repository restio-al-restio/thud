#!/bin/sh
#
# Copyright (C) 2005, 2008-2021 Thien-Thi Nguyen
#
# This file is part of THUD.
#
# THUD is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# THUD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with THUD.  If not, see <http://www.gnu.org/licenses/>.

if [ x"$1" = x--verbose ] ; then
    verbose=$1
    shift
fi

# Another way is to use gnulib's config/srclist-update.
actually ()
{
    gnulib-tool --copy-file $1 $2
}
actually doc/INSTALL.UTF-8 INSTALL
actually build-aux/install-sh

set -e
guile-baux-tool snuggle m4 build-aux
aclocal $verbose -I build-aux -I $HOME/local/share/aclocal
mv aclocal.m4 build-aux
autoconf $verbose -I build-aux

# autogen.sh ends here
