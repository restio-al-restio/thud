;;; (thud sched) --- Dependency extraction and static scheduling

;; Copyright (C) 1998-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Compiling this module w/ hobbit results in runtime errors (not yet
;; fully investigated), so don't do that for now.

;;; Code:

(define-module (thud sched)
  #:export (set-schedules!)
  #:use-module ((thud com) #:select (top-level-name?
                                     dbg fln die assert
                                     vn-var))
  #:use-module ((thud types) #:select (d->list
                                       d-ref
                                       make-dn dn-obj dn-back dn-inm
                                       vn-bw vn-enof vn-rep vn-deps
                                       enof-deps
                                       instance-values
                                       make-en en-dn eform
                                       PORT? flop?
                                       instance-blk
                                       blk-ref?
                                       in-port?))
  #:use-module ((thud state) #:select (k make-context))
  #:use-module ((thud scan) #:select (check-exp)))

(define dbg-sched #f)

;;;---------------------------------------------------------------------------
;;; Topological sort

(define-macro (dolist setup . body)
  `(for-each (lambda (,(car setup)) ,@body) ,(cadr setup)))

(define (topological-sort ls suppress)
  (let* ((bc (make-object-property))    ; back edge count
         (fw (make-object-property))    ; forward edges
         (ok (list #f))                 ; ready-to-emit nodes queue
         (ow ok)                        ; write pointer
         (rb (list #f))                 ; return value box
         (rw rb))                       ; write pointer

    (define (o+! x)
      (set-cdr! ow (cons x '()))
      (set! ow (cdr ow)))

    ;; find forward edges, note ready-to-emit nodes
    (dolist (name/backs ls)
            (let ((name (car name/backs))
                  (count 0))
              (or (suppress name)
                  (dolist (back (cdr name/backs))
                          (set! count (1+ count))
                          (cond ((fw back)
                                 => (lambda (prev)
                                      (set! (fw back) (cons name prev))))
                                (else
                                 (set! (fw back) (cons name '()))))))
              (if (zero? count)
                  (o+! name)
                  (set! (bc name) count))))

    ;; emit (or suppress) nodes w/ no back edges
    (let loop ((ok (cdr ok)))
      (if (null? ok)
          (cdr rb)                      ; rv
          (let ((emitted (car ok)))
            (cond ((suppress emitted))
                  (else (set-cdr! rw (cons emitted '()))
                        (set! rw (cdr rw))))
            ;; decrement back edge count of forward nodes
            (and=> (fw emitted)
                   (lambda (forward)
                     (dolist (f forward)
                             (set! (bc f) (1- (bc f)))
                             ;; no more back edges => ready-to-emit!
                             (and (zero? (bc f))
                                  (o+! f)))))
            (loop (cdr ok)))))))


;;;---------------------------------------------------------------------------
;;; Form raw schedule by ordering the instance pool

(define (find-deps iname blk oname vn)
  (and dbg-sched (fln "find-deps: ~S ~S" oname vn))

  (let ((enof (vn-enof vn))
        (rep  (vn-rep  vn)))

    (define (changed r)
      (if (pair? r)
          (if (list? r)
              (map changed r)
              (if (symbol? (cdr r))
                  (symbol-append iname (car r) (k #:inm-term-sym) (cdr r))
                  (cons (changed (car r)) (changed (cdr r)))))
          r))

    (define (fullpath s) (if (top-level-name? s) s (symbol-append iname s)))

    (set! rep (changed rep))
    (set! (vn-rep vn) rep)              ; hmm

    ;; this part could still use some rationalization!
    (cond ((in-port? enof)
           (and dbg-sched (fln "find-deps: ID as in port: ~S" vn))
           (if (not (blk-ref? blk))
               '()
               (begin
                 (assert rep)
                 (if (symbol? rep)
                     (list rep)
                     (list (cadr rep))))))
          ((PORT? enof)                 ; must be out-port
           (let ((deps (enof-deps (vn-enof vn))))
             (or deps (set! deps '()))  ; fault-tolerance
             (and dbg-sched (fln "find-deps: ID as out port: ~S ~S" vn deps))
             (if (null? deps)
                 (check-exp iname blk rep)
                 (map fullpath deps))))
          (else
           (let ((deps (enof-deps (vn-enof vn))))
             (or deps (set! deps '()))  ; fault-tolerance
             (and dbg-sched (fln "find-deps: ID as non port: ~S ~S" vn deps))
             (if rep
                 (if (null? deps)
                     (check-exp iname blk rep)
                     (map fullpath deps))
                 '()))))))

;; For each evaluatable net or flop in the pool, canonicalize dependencies,
;; create new dependency nodes and add them to the appropriate schedule.
;;
(define (form-raw-schedule session scratch)
  (let ((loc (make-context '((#:backs #t 431)
                             (#:flops #t 431)))))

    (for-each
     (lambda (nm-instance)
       (and dbg-sched (fln "iname: ~S" (car nm-instance)))
       (let* ((iname    (car nm-instance))
              (instance (cdr nm-instance)))

         (define (fullpath s) (symbol-append iname s))

         (for-each
          (lambda (nm-vn)
            (and dbg-sched (fln "nm-vn: ~S" nm-vn))
            (let* ((oname (car nm-vn))
                   (vn    (cdr nm-vn))
                   (enof  (vn-enof vn))
                   (fulln (fullpath oname))
                   (deps  (find-deps iname (instance-blk instance) oname vn))
                   (dn    (make-dn fulln deps iname)))
              (set! (loc #:backs fulln) dn)
              (cond ((flop? enof)
                     (set! (loc #:flops fulln) dn)
                     (set! (session #:flops fulln) #t)))
              (set! (vn-deps fulln) deps)))
          (d->list
           (instance-values instance)))))
     (session list #:instances))

    (set! (scratch #:net-sched)
          (map (lambda (sym)
                 (loc #:backs sym))
               (topological-sort (map (lambda (fulln/dn)
                                        (cons (car fulln/dn)
                                              (dn-back (cdr fulln/dn))))
                                      (loc list #:backs))
                                 (lambda (sym)
                                   (loc #:flops sym)))))

    (set! (scratch #:flop-sched)
          (map cdr (loc list #:flops)))))


;;;---------------------------------------------------------------------------
;;; Finalize the schedule
;;;
;;; Create one schedule from the net and flop schedules, transforming
;;; references to fullpath forms.  The unified schedule is made of execution
;;; nodes, which keep dependency information.
;;;
;;; The transforms are either left-hand side, right-hand side or pertaining to
;;; iref-ports.  The dispatch `rep-transform' only uses `rhs-transform' and
;;; `irp-transform' and must know about the `bbb' special form to be able to
;;; distinguish.  [TODO: Clean up bbb.]
;;;
;;; Because flops must be seen to evaluate in parallel, we need to arrange for
;;; the runtime to read current state, write next state, and finally after
;;; evaluation, move next state to current state.  This is done by using a
;;; macro other than `set!' (but sharing formals) which saves next state in an
;;; object property.

;; Create one schedule from the net and flop schedules,
;; transforming references to fullpath forms.  The unified
;; schedule is made of execution nodes, which keep dependency
;; information.
;;
(define (finalize-schedule session scratch)
  (let* ((/-sym (k #:inm-term-sym))
         (jam! 'set!)
         (duty-box (list #f))
         (tp duty-box))

    (define (duty+! x)
      (set-cdr! tp (cons x '()))
      (set! tp (cdr tp)))

    (define (finalize! dn)
      (let* ((iname         (dn-inm dn))
             (instance      (session #:instances iname))
             (lhs-transform (dn-obj dn)))

        (define (rhs-transform s)
          (if (d-ref (instance-values instance) s)
              (symbol-append iname s)
              s))

        (define (irp-transform iref-irp) ; (iref-nm . irp-nm)
          (and dbg-sched (fln "irp-transform: ~S" iref-irp))
          (let* ((iref-nm  (car iref-irp))
                 (irp-nm   (cdr iref-irp))
                 (iref-inm (symbol-append iname iref-nm /-sym)))
            (symbol-append iref-inm irp-nm)))

        (define (rep-transform exp)
          (if (list? exp)
              (cons (car exp) (map rep-transform (cdr exp)))
              (cond ((not exp) #f)
                    ((number? exp) exp)
                    ((pair? exp) (irp-transform exp))
                    (else (rhs-transform exp)))))

        (duty+! (make-en `(,jam! ,lhs-transform
                                 ,(rep-transform
                                   (vn-rep lhs-transform)))
                         dn))))

    (for-each finalize! (scratch #:net-sched))
    (set! jam! 'fnext!)
    (for-each finalize! (scratch #:flop-sched))
    (set! (session #:duty) (cdr duty-box))))

;;;---------------------------------------------------------------------------
;;; Optimize the schedule
;;;
;;; Collapse execution nodes which are actually the same (one and only one
;;; signal in its representation).  Also, take this opportunity to build make
;;; forward edges, indicating influence on subsequent nodes, to enable dynamic
;;; scheduling.  The "first flop" encountered is also remembered in order to
;;; effectively evaluate the flops in parallel.  (See the simulation section.)

;; Reverse walk the todo list, setting `infl' pointers for all `deps' back to
;; the current node.
;;
;; If the node has already been identified as having singular representation
;; (an `aka' node), pass on its `infl' pointers to the actual node.
;;
;; Otherwise, for new `aka' nodes do the following: point to actual node
;; through `aka' property; set value in `(session #:vob)' to actual node's
;; value; pass on `infl' pointers to actual node; delete all properties
;; besides `aka'.
;;
;; Lastly, form a new todo list, removing `aka' nodes (but save them in
;; `(session #:aliases)' for possible later use).
;;
(define (separate-aka-nodes-noting-influence session)
  (let ((infl (make-object-property))
        (reversed-todo-list (reverse (session #:duty)))
        (working-list '()))
    (define (maybe-collapse en)
      (let* ((start-vn  (dn-obj (en-dn en)))
             (known-aka (session #:aliases start-vn)))
        (if known-aka
            (set! (infl known-aka) (append (or (infl known-aka) '())
                                           (or (infl start-vn)  '())))
            (begin
              (for-each (lambda (dep)
                          (set! (infl dep) (cons start-vn (or (infl dep) '()))))
                        (vn-deps start-vn))
              (let loop ((vn     start-vn)
                         (ghosts #f))
                (let* ((rep (vn-rep  vn))
                       (dep (vn-deps vn))
                       ;; Determine if this node is an alias to be deleted from
                       ;; the todo list.  Flops are never deleted.
                       (bye (and (not (flop? (vn-enof vn)))
                                 rep (if (list? rep)
                                         (and (eq? '() (cdr rep)) 'one)
                                         (if (pair? rep)
                                             'out
                                             (and (symbol? rep) ; consts safe
                                                  'one)))))
                       (aka (and bye (car dep))))
                  (cond (bye
                         (loop aka (cons vn (or ghosts '()))))
                        (ghosts
                         (for-each (lambda (ghost)
                                     (set! (session #:aliases ghost) vn)
                                     (set! (session #:vob ghost) (vn-var vn))
                                     (set! (infl vn) (append
                                                      (or (infl ghost) '())
                                                      (or (infl vn)    '()))))
                                   ghosts))
                        (else
                         (set! working-list (cons en working-list))))))))))

    (for-each maybe-collapse reversed-todo-list)
    (set! (session #:duty)
          ;; Substitute aliases in execution forms.
          (let ((aliases (session list #:aliases)))
            (define (replace x)
              (cond ((pair? x) (map replace x))
                    ((assq-ref aliases x))
                    (else x)))
            (map (lambda (en)
                   (make-en (replace (eform en)) (en-dn en)))
                 working-list)))))

;; (todoc)
;;
(define (optimize-schedule session)

  (define (check-no-forward-dep some-todo-list)
    (let loop ((ls (map en-dn some-todo-list)))
      (or (not (pair? ls))
          (let ((following (map dn-obj (cdr ls))))
            (for-each (lambda (dep)
                        (assert (or (flop? (vn-enof dep))
                                    (not (memq dep following)))))
                      (dn-back (car ls)))
            (loop (cdr ls))))))

  (define (spew! msg)
    ;; like `display-todo-list-reps' but does not respect scope
    (fln msg)
    (let loop ((idx 0) (ls (session #:duty)))
      (if (not (pair? ls))
          (newline)
          (let* ((dn (en-dn (car ls)))
                 (vn (dn-obj dn)))
            (fln "~S ~S\t~S ~S" idx (vn-bw vn) (dn-obj dn) (vn-rep vn))
            (loop (+ 1 idx) (cdr ls))))))

  (dbg DBGSCH
       (spew! "[-d] before schedule optimization:")
       (and (check-no-forward-dep (session #:duty))
            (fln "[-d] good! no forward deps, todo list length ~S"
                 (length (session #:duty)))))

  (separate-aka-nodes-noting-influence session)

  (dbg DBGSCH
       (spew! "[-d] after schedule optimization:")
       (fln "[-d] aliases:")
       (for-each (lambda (pair) (fln "~S" pair)) (session list #:aliases))
       (and (check-no-forward-dep (session #:duty))
            (fln "[-d] good! no forward deps, todo list length ~S"
                 (length (session #:duty))))))


;;;---------------------------------------------------------------------------
;;; Set the schedules!

(define (set-schedules! session)
  (dbg DBGGEN (fln "[-d] Setting schedules"))
  (let ((scratch (make-context '((#:net-sched #f #f)
                                 (#:flop-sched #f #f)))))
    (form-raw-schedule session scratch)
    (finalize-schedule session scratch)
    (optimize-schedule session)))

;;; (thud sched) ends here
