;;; (thud state) --- Global state

;; Copyright (C) 1998-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; All global state is declared here, including constants and variables.

;;; Code:

(define-module (thud state)
  #:export (k                           ; constants
            session                     ; session state
            make-context                ; machinery
            global-state-reset!)        ; reset
  #:use-module ((thud types) #:select (make-dic d-set! d-ref d->list)))

;;;---------------------------------------------------------------------------
;;; Constants

(define *k*
  `(
    ;; Debugging support.
    ;;
    (DBGGEN . ,(ash 1 0))               ; general stuff
    (DBGSYM . ,(ash 1 1))               ; internal symbol transfer
    (DBGUSR . ,(ash 1 2))               ; user stuff
    (DBGTHR . ,(ash 1 3))               ; reading of TH source
    (DBGSCH . ,(ash 1 4))               ; scheduling
    (DBGDMP . ,(ash 1 5))               ; dump-related stuff
    (DBGSIM . ,(ash 1 6))               ; simulation stuff
    (DBGALL . ,(1- (ash 1 7)))          ; => (1- 128) => 127

    ;; Instance names always end in `inm-term'.  This should be a short
    ;; (preferably one-character) string.  To get "dot" names, use "." and
    ;; something like "TOP-LEVEL." (note the trailing dot).
    ;;
    (#:inm-term . "/")                  ; instance-name terminator
    (#:inm-term-sym . /)                ;   ... again, as symbol
    (#:top-level-name . "/")            ; must end with `inm-term'
    (#:top-level-name-re . ,(make-regexp "/"))

    ;; Operator map consulted by scan.scm and sim.scm.
    ;;
    (#:op-map . ((^  . thud-logxor)
                 (%  . thud-modulo)
                 (!  . thud-invert)
                 (~  . thud-invert)
                 (+  . thud-plus)
                 (-  . thud-minus)
                 (*  . thud-mult)
                 (/  . thud-div)
                 (&  . thud-logand)
                 (|  . thud-logior)     ;|
                 (=  . thud-equal)
                 (<< . thud-shl)
                 (>> . thud-shr)))))

(define k
  (let ((dictionary (make-dic (length *k*))))
    (for-each (lambda (pair)
                (d-set! dictionary (car pair) (cdr pair)))
              *k*)
    ;; rv
    (lambda (key)
      (d-ref dictionary key))))

;;;---------------------------------------------------------------------------
;;; Contexts

(define (apply-to-args args proc) (apply proc args)) ; FIXME: Centralize.

;; Return a "context" that manages a dictionary described by STATE-SPECS.
;; Each state-spec is a specification: (NAME DICT? INIT).  NAME is a keyword.
;; If DICT? is #t, initialize NAME's entry w/ a dictionary of size INIT and
;; arrange to get/set using the dictionary procedures, otherwise initialize
;; with INIT and arrange to get/set directly as a normal variable.
;;
(define (make-context state-elements)
  (let* ((count (length state-elements))
         (state (make-dic count))
         (reset (make-dic count))
         (dictionaries (make-dic count)))
    (for-each (lambda (se)
                (apply-to-args
                 se (lambda (name dict? init)
                      (d-set! reset name init)
                      (cond (dict?
                             (d-set! dictionaries name #t)
                             (d-set! state name (make-dic init)))
                            (else
                             (d-set! state name init))))))
              state-elements)
    ;; rv
    (make-procedure-with-setter
     ;; getter
     (lambda (name . args)
       (if (eq? list name)              ; list dictionaries
           (d->list (d-ref state (car args)))
           (let ((v (d-ref state name)))
             (if (d-ref dictionaries name)
                 (d-ref v (car args))
                 v))))
     ;; setter
     (lambda (name . args)
       (if (not name)
           (let* ((name (car args))
                  (init (or (and (not (null? (cdr args)))
                                 (cadr args))
                            (d-ref reset name))))
             (d-set! state name (if (d-ref dictionaries name)
                                    (make-dic init)
                                    init)))
           (if (d-ref dictionaries name)
               (apply d-set! (d-ref state name) args)
               (d-set! state name (car args))))))))

;;;---------------------------------------------------------------------------
;;; Common bits of context

(define *options*
  ;;
  ;; Set during invocation.
  ;;
  '((#:opt-batch     #f #f)
    (#:opt-verbose   #f #t)
    (#:opt-debug     #f  0)
    (#:opt-init-file #f #f)
    (#:opt-face      #f #f)))

(define *pools*
  ;;
  ;; Pools are dictionaries.
  ;;
  '((#:blocks    #t 431)                ; all blocks
    (#:instances #t 431)                ; all instances
    (#:aliases   #t 431)                ; all aliases
    (#:flops     #t 431)                ; all flops
    (#:dangling  #t 431)))              ; ref w/o defs (scan)

(define *schedules*
  ;;
  ;; Schedules and lists are ordered lists.
  ;;
  '((#:duty #f ())                      ; evaluation nodes
    (#:TODO #f ())))                    ; what to do

(define *cycle-hooks*
  ;;
  ;; Each is run at the obvious time by `run-hook'.
  ;; (Actually, what is "obvious"?)
  ;;
  '((#:bef-cycle #f #f)
    (#:aft-cycle #f #f)))

(define *ui*
  ;;
  ;; Face stuff.
  ;;
  `((#:scope #f ,(k #:top-level-name)))) ; prefix

(define *runtime*
  ;;
  ;; Data needed to execute the simulation.
  ;;
  '((#:vob        #t 431)               ; values (variables) obarray
    (#:log        #f ())                ; event log
    (#:cmax       #f #f)                ; maximum cycle
    (#:vcd-mangle #f #f)                ; VCD output mangler
    (#:vcd-file   #f "thud.dump")))     ; where to send VCD output

;;;---------------------------------------------------------------------------
;;; Fixed (for now) global context

(define session (make-context (append *options*
                                      *pools*
                                      *schedules*
                                      *cycle-hooks*
                                      *ui*
                                      *runtime*)))

;;;---------------------------------------------------------------------------
;;; Get to known state
;;;
;;; Note that options *are not* included here.

(define (global-state-reset!)
  (set! (session #f #:blocks) #f)
  (set! (session #f #:instances) #f)
  (set! (session #f #:aliases) #f)
  (set! (session #f #:flops) #f)
  (set! (session #f #:dangling) #f)
  (set! (session #:duty) '())
  (set! (session #:TODO) '())
  (set! (session #f #:vob) #f)
  (set! (session #:bef-cycle) (make-hook))
  (set! (session #:aft-cycle) (make-hook))
  (set! (session #:log) '())
  (set! (session #:vcd-file) "thud.dump")
  (set! (session #:scope) (k #:top-level-name)))

;;; (thud state) ends here
