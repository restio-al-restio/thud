;;; (thud scan) --- Read THUD HDL

;; Copyright (C) 1998-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Implementation Note: `define-syntax' forms last seen 0.22.

;;; Code:

(define-module (thud scan)
  #:export (check-exp
            populate-instance-pool
            add-th-file)
  #:use-module ((ice-9 and-let-star) #:select (and-let*))
  #:use-module ((thud types) #:select (make-dic
                                       d-set! d-ref d->list
                                       make-port make-net make-flop
                                       in-port?
                                       enof-bw enof-rep pdir
                                       enof-rep-set! enof-deps-set!
                                       make-iref iref-blk iref-box iref-maps
                                       make-blk make-incomplete-blk
                                       blk-ports blk-nets blk-flops blk-irefs
                                       blk-ref? blk-ports-set! blk-ref?-set!
                                       make-vn vn-enof vn-rep vn-deps
                                       make-instance
                                       instance-values instance-values-set!))
  #:use-module ((thud state) #:select (k session))
  #:use-module ((thud com) #:select (fln dbg die assert))
  #:use-module ((thud evlog) #:select (log-event-verbosely
                                       log-event)))

;;;---------------------------------------------------------------------------
;;; Find things in dictionaries
;;;

(define (find-port blk name) (d-ref (blk-ports blk) name))
(define (find-net  blk name) (d-ref (blk-nets  blk) name))
(define (find-flop blk name) (d-ref (blk-flops blk) name))
(define (find-iref blk name) (d-ref (blk-irefs blk) name))



;;;---------------------------------------------------------------------------
;;; Support for bit slicing
;;;

;; Clean expressions by translating "[X]" syntax into calls to bit-extract
;; primitives.  If X is A:B, use the multi-bit extractor `Bx', which takes
;; three args: NAME, BITPOS and LENGTH.  Otherwise X is simply A, and we use
;; the single-bit extractor `bx', which takes two args: NAME and BITPOS.
;;
(define (cleaned exp)
  (define (sub->n s b e)
    (string->number (substring s b e)))
  (cond ((not exp) #f)
        ((pair? exp)
         (case (car exp)
           ((fln)                       ; transform: arg -> (or arg "x")
            `(fln ,(cadr exp)
                  ,@(map (lambda (arg)
                           (if (equal? '(cc) arg) ; but not current cycle
                               arg
                               `(or ,arg "x")))
                         (cddr exp))))
           (else
            (if (pair? (cdr exp))
                (map cleaned exp)
                (cons (cleaned (car exp)) (cleaned (cdr exp)))))))
        ((and (symbol? exp)
              (and-let* ((s (symbol->string exp))
                         (lb (string-index s #\[))
                         (rb (string-rindex s #\]))
                         (sig (string->symbol (substring s 0 lb))))
                (cond ((string-rindex s #\: 0 rb)
                       => (lambda (c)
                            (let ((hi (sub->n s (1+ lb) c))
                                  (lo (sub->n s (1+ c) rb)))
                              `(Bx ,sig ,lo ,(- hi lo)))))         ; long
                      (else `(bx ,sig ,(sub->n s (1+ lb) rb))))))) ; short
        (else exp)))                                               ; untouched



;;;---------------------------------------------------------------------------
;;; Check port-matching for irefs
;;; Mappings must have valid endpoints.
;;; If the box endpoint is a port, make sure it's compatible.
;;;

(define (check-ports iref-name iref)
  (dbg DBGTHR (fln "check-ports: ~S ~S" iref-name iref))

  (let* ((blk (iref-blk iref))
         (box (iref-box iref))
         (box-wires (append (d->list (blk-ports box))
                            (d->list (blk-nets  box))
                            (d->list (blk-flops box)))))

    ;; nm-map can either be (iref-pname . box-pname)
    ;;                   or (iref-pname BIT-EXTRACT-FORM)
    ;; BIT-EXTRACT-FORM can be either (bx sig bit) or (Bx sig bit len)
    ;;
    (define (check-iref-map nm-map)
      (let* ((iref-pname (car nm-map))
             (v          (cdr nm-map))
             (box-pname  (if (pair? v) (cadr v) v))
             (box-wire   (assoc box-pname box-wires))
             (iref-port  (d-ref (blk-ports blk) iref-pname))
             (box-port   (d-ref (blk-ports box) box-pname)))

        (or iref-port (die "no such port:" iref-pname))
        (or box-wire  (die "no such wire:" box-pname))

        (set! box-wire (cdr box-wire))  ; now an `enof', like `iref-port'

        (and box-port
             (eq? 'out (pdir iref-port))
             (eq? 'in  (pdir box-port))
             (die "bad cnxn: box in-port" box-pname
                  "to iref out-port" iref-pname))

        (or (= (if (pair? v)
                   (case (car v)
                     ((bx) 1)
                     ((Bx) (cadddr v))
                     (else (assert #f)))
                   (enof-bw box-wire))
               (enof-bw iref-port))
            (die "port width mismatch:" iref-pname "=" (enof-bw iref-port)
                 "and" box-pname "=" (enof-bw box-wire)))))

    (or (and blk box (blk-ports blk) (blk-ports box))
        (throw 'thud-undefined-blk))

    (for-each check-iref-map (d->list (iref-maps iref)))))



;;;---------------------------------------------------------------------------
;;; Build internal structures
;;;

;; Add to `(session #:blocks)', if necessary (a block may be
;; forward-referenced, see `add-iref').  Return the blk.
;;
(define (add-blk name port-list)
  (dbg DBGTHR (fln "add-blk: ~S ~S" name port-list))
  (let ((blk (or (and=> (session #:blocks name)
                        (lambda (predef)
                          (and (blk-ports predef)
                               (die "already defined blk:" name))
                          (blk-ports-set! predef (make-dic))
                          predef))
                 (set! (session #:blocks name) (make-blk)))))
    (for-each (lambda (nm-port)
                (d-set! (blk-ports blk) (car nm-port) (cdr nm-port)))
              port-list)
    blk))

;; Add nets and flops to `biq'.
;;
(define (add-net biq name width)
  (dbg DBGTHR (fln "add-net: ~S ~S" name width))
  (if (find-net biq name)
      (die "already used wire:" name)
      (d-set! (blk-nets biq) name (make-net width))))
(define (add-flop biq name width)
  (if (find-flop biq name)
      (die "already used flop:" name)
      (d-set! (blk-flops biq) name (make-flop width))))

;; Add iref to `biq'.
;;
;; If blk is already defined, call `check-ports' on iref.  Otherwise, keep
;; track of it via `(session #:dangling)' and add blk to `(session #:blocks)',
;; with ports set to #f.  See `check-ports'.
;;
;; In all cases, before adding, check to see nets have already been created.
;;
(define (add-iref biq blk-name iref-name args)
  (dbg DBGTHR (fln "add-iref: ~S ~S ~S" blk-name iref-name args))
  (set! args (map cleaned args))
  (dbg DBGTHR (fln "cleaned args: ~S" args))
  (and (find-iref biq iref-name)
       (die "already used instance-name:" iref-name))
  (let* ((predef (session #:blocks blk-name))
         (blk (or predef (make-incomplete-blk)))
         (new-iref (make-iref blk biq args)))
    (or predef
        (set! (session #:blocks blk-name) blk))
    (blk-ref?-set! blk #t)              ; this blk has been iref'ed!
    (if (and predef (blk-ports blk))
        (check-ports iref-name new-iref)
        (set! (session #:dangling iref-name) new-iref))

    ;; args can either be (iref-port . wire)
    ;;                 or (iref-port . BIT-EXTRACT-FORM)
    ;; BIT-EXTRACT-FORM is either (bx sig bit) or (Bx sig bit len)
    ;;
    (for-each
     (lambda (irp-w)
       (or (and (pair? irp-w)
                (if (list? irp-w)
                    (or (eq? 'bx (cadr irp-w)) (eq? 'Bx (cadr irp-w)))
                    #t))
           (die "invalid instance port spec:" irp-w))
       (let* ((irp-name (car irp-w))
              (irp-out? (and predef
                             (let* ((ports (blk-ports blk))
                                    (irp (and ports (d-ref ports irp-name))))
                               (and irp
                                    (eq? 'out (pdir irp))))))
              (w    (cdr irp-w))
              (wire (if (pair? w) (cadr w) w))
              (port                 (find-port biq wire))
              (net  (and (not port) (find-net  biq wire)))
              (flop (and (not net)  (find-flop biq wire)))
              (enof (or port flop net
                        (die "no such wire:" wire))))
         (if irp-out?                   ; need to set rep
             (begin
               (dbg DBGTHR
                    (fln "irp output port: ~S goes to wire: ~S" irp-name wire))
               (and port
                    (eq? 'in (pdir port))
                    (die "in port map" irp-w "wire" wire "is an input port,\n"
                         " but instance port" irp-name "is an output port"))
               (define-a. biq w (cons iref-name irp-name))))))
     args)
    (d-set! (blk-irefs biq) iref-name new-iref)))



;;;---------------------------------------------------------------------------
;;; Handle `a.' and `n.' constructs
;;;

(define (define-a. biq name exp)
  (dbg DBGTHR (fln "define-a. ~S ~S" name exp))
  (set! exp (cleaned exp))
  (dbg DBGTHR (fln "cleaned exp: ~S" exp))
  (let* ((bit-assign (list? name))
         (nm  (or (and bit-assign (cadr name))
                  name))
         (net (or (find-net biq nm)
                  (let ((port (find-port biq nm)))
                    (and port
                         (if (eq? (pdir port) 'in)
                             (die "cannot assign to input port:" nm)
                             port))))))
    (cond ((eq? #f net)
           (die "no such net:" nm))
          (bit-assign
           (let* ((hi-lo (eq? 'Bx (car name)))
                  (bit   (caddr name))
                  (len   (and hi-lo (cadddr name)))
                  (cur-rep (or (enof-rep net)
                               `(*bofh* ,(enof-bw net)))))
             (enof-rep-set! net
                            (cons (car cur-rep)                 ; hmm
                                  (cons (cadr cur-rep)          ; hmm
                                        (append (if hi-lo
                                                    (list exp (- len) bit)
                                                    (list exp bit))
                                                (cddr cur-rep)))))))
          (else
           (if (enof-rep net)
               (die "port or net already defined:" name "\n"
                    " definition:" (enof-rep net))
               (enof-rep-set! net exp))))))

(define (define-n. biq name exp)
  (let ((flop (find-flop biq name)))
    (if (eq? #f flop)
        (die "no such flop:" name)
        (if (enof-rep flop)
            (die "flop already defined:" name "\n"
                 " definition:" (enof-rep flop))
            (enof-rep-set! flop exp)))))


;;;---------------------------------------------------------------------------
;;; Checks (w/ side effects)
;;;

;; Check iref forward-references.
;;
(define (check-refs-no-defs)
  (for-each (lambda (nm-iref)
              (let ((iref (cdr nm-iref)))
                (catch 'thud-undefined-blk
                       (lambda ()
                         (dbg DBGTHR
                              (fln "check-refs-no-defs: checking ports: ~S"
                                   (car nm-iref)))
                         (check-ports (car nm-iref) iref))
                       (lambda args
                         (die "undefined blk for iref:" (car nm-iref))))))
            (session list #:dangling)))

;; Check expression for proper refs.
;;
;; If OK, return list of other nets and flops used; otherwise signal error.
;;
(define ok-syms
  (append
   ;; operators
   (map car (k #:op-map))
   ;; keywords
   '(bx Bx begin c. case cond if or and die fln
        eq? else bx Bx begin not dump n!)))

(define iname?
  (let* ((inm-term (k #:inm-term))
         (len (string-length inm-term)))
    (lambda (symbol)
      (string=? inm-term (substring (symbol->string symbol) 0 len)))))

;; Check to see if in @var{blk}, @var{expression} is valid.
;; Throw @code{thud-bad-exp} if not.
;;
(define (check-exp who blk expression)
  (let ((deps '()))
    (dbg DBGTHR (fln "check-exp: (~S) ~S" who expression))
    (catch
     'thud-bad-exp
     (lambda ()
       (let loop ((exp expression))
         (or (and (not (list? exp))
                  (or (memq exp deps)
                      (and (pair? exp)
                           (find-iref blk (car exp))) ; todo: check cdr
                      (memq exp ok-syms)
                      (and (eq? exp 'cc)
                           (set-object-property! expression 'func-of-cc #t)
                           #t)
                      (string? exp)
                      (number? exp)
                      (and (or (iname? exp)
                               (find-port blk exp)
                               (find-net  blk exp)
                               (find-flop blk exp))
                           (set! deps (cons exp deps)))
                      (throw 'thud-bad-exp exp)))
             (for-each (lambda (x)
                         (if (not (loop x))
                             (throw 'thud-bad-exp exp)))
                       (case (car exp)  ; handle special forms
                         ((*bofh*)
                          (cdr exp))
                         (else
                          exp))))))
     (lambda (key exp)
       (die who "-- bad expression:" exp)))
    deps))

(define (check-evaluatables block-name blk)

  (define (check-nm-enof nm-enof)
    (let* ((name (car nm-enof))
           (enof (cdr nm-enof))
           (rep  (enof-rep enof)))
      (if (not rep)
          (fln "WARNING! blk ~S signal ~S has no definition!" block-name name)
          (enof-deps-set! enof (check-exp block-name blk (enof-rep enof))))))

  (define (check-aspect aspect . except)
    (let ((ignore (if (null? except)
                      (lambda ignored #f)
                      (car except))))
      (for-each (lambda (nm-enof)
                  (or (ignore nm-enof)
                      (check-nm-enof nm-enof)))
                (d->list (aspect blk)))))

  ;; do it!
  (dbg DBGTHR (fln "check-evaluatables: ~A" block-name))
  (check-aspect blk-ports (lambda (nm-port)
                            (eq? 'in (pdir (cdr nm-port)))))
  (check-aspect blk-nets)
  (check-aspect blk-flops))


;;;---------------------------------------------------------------------------
;;; Instantiate to top level
;;;

;; Check irefs.
;; Check expresssions.
;; Populate @code{(session #:instances)} value instances.
;;
(define (populate-instance-pool)

  (define (make-bound-vn name enof rep)
    (set! (session #:vob name) (make-variable #f))
    (make-vn name enof rep))

  (define (instantiate-blk blk parent-name my-name)
    (dbg DBGTHR (fln "instantiate-blk: ~S ~S" parent-name my-name))
    (let* ((instance-name (symbol-append parent-name my-name
                                         (k #:inm-term-sym)))
           (instance (make-instance blk)))
      (define (add-enof-entry nm-enof)
        (dbg DBGTHR (fln "add-enof-entry: ~S" nm-enof))
        (let* ((name (car nm-enof))
               (enof (cdr nm-enof))
               (vn   (make-bound-vn (symbol-append instance-name name)
                                    enof (enof-rep enof))))
          (d-set! (instance-values instance) name vn)))

      (define (add-iref-entry nm-iref)
        (dbg DBGTHR (fln "add-iref-entry: ~S" nm-iref))
        (let ((new-blk (instantiate-blk (iref-blk (cdr nm-iref)) ; recurse
                                        instance-name
                                        (car nm-iref))))
          ;; create value nodes for child input ports
          ;; map can either be (iref-pname . box-pname)
          ;;                or (iref-pname . BIT-EXTRACT-FORM)
          ;; BIT-EXTRACT-FORM is either (bx sig bit) or (Bx sig bit len)
          ;;
          (for-each
           (lambda (map)
             (let* ((iref-pname (car map))
                    (cdr-map    (cdr map))
                    (bx-form    (pair? cdr-map))
                    (box-pname  ((if bx-form caddr cdr) map))
                    (cvn (d-ref (instance-values new-blk) ; child vn
                                iref-pname)))
               (and (in-port? (vn-enof cvn))
                    (let* ((enof (or (find-port blk box-pname)
                                     (find-net  blk box-pname)
                                     (find-flop blk box-pname)))
                           (pvn (make-bound-vn
                                 (symbol-append instance-name box-pname)
                                 enof (enof-rep enof))))
                      (set! (vn-rep cvn)
                            (if bx-form
                                (cons (car cdr-map)
                                      (cons pvn (cddr cdr-map)))
                                pvn))
                      (assert (symbol? pvn))
                      (set! (vn-deps cvn) (list pvn))))))
           (d->list
            (iref-maps (cdr nm-iref))))))

      (instance-values-set! instance (make-dic))

      (for-each add-enof-entry (d->list (blk-ports blk)))
      (for-each add-enof-entry (d->list (blk-nets  blk)))
      (for-each add-enof-entry (d->list (blk-flops blk)))
      (for-each add-iref-entry (d->list (blk-irefs blk)))

      (set! (session #:instances instance-name) instance)

      instance))

  (define instantiate-blk-at-top-level
    (let ((top (string->symbol (k #:top-level-name))))
      ;; instantiate-blk-at-top-level
      (lambda (nm-blk)
        (dbg DBGTHR (fln "instantiate-blk-at-top-level: ~S" (car nm-blk)))
        (let ((name (car nm-blk))
              (blk (cdr nm-blk)))
          (or (blk-ref? blk)
              (instantiate-blk blk top name))))))

  ;; OK, do it!
  ;;
  (let ((blks (session list #:blocks)))
    (check-refs-no-defs)
    (for-each (lambda (nm-blk)
                (dbg DBGTHR (fln "checking blk: ~S" (car nm-blk)))
                (check-evaluatables (car nm-blk) (cdr nm-blk)))
              blks)

    (for-each instantiate-blk-at-top-level blks))

  ;; clean up
  (set! (session #f #:dangling) 1)
  (gc))


;;;---------------------------------------------------------------------------
;;; Loading a THUD file
;;;

;; Add design (RTL) @var{filename} to the workspace.
;;
(define (add-th-file filename)
  (log-event-verbosely 'adding filename)
  (call-with-input-file filename
    (lambda (p)
      (call-with-current-continuation
       (lambda (return)
         (let ((biq #f))                ; "block in question"
           (define (scan-dispatch form)
             (define (a1) (cadr  form))
             (define (r1) (cddr  form))
             (define (a2) (caddr form))
             (define (r2) (cdddr form))
             (define (a3) (cadddr form))

             (let ((len (length form)))
               (case (car form)

                 ;; (blk name port-list)
                 ;;
                 ((blk)  (let ((name (a1))
                               (port-list (map scan-dispatch (r1))))
                           (set! biq (add-blk name port-list))))

                 ;; (in bit-width name)
                 ;; (in name)
                 ;;
                 ((in)   (if (number? (a1))
                             (cons (a2) (make-port (a1) 'in))
                             (cons (a1) (make-port 1 'in))))

                 ;; (out bit-width name)
                 ;; (out name)
                 ;;
                 ((out)  (if (number? (a1))
                             (cons (a2) (make-port (a1) 'out))
                             (cons (a1) (make-port 1 'out))))

                 ;; (wire bit-width name def)
                 ;; (wire bit-width name)
                 ;; (wire name def)
                 ;; (wire name)
                 ;;
                 ((wire) (if (number? (a1))
                             (begin
                               (add-net biq (a2) (a1))
                               (and (> len 3)
                                    (define-a. biq (a2) (a3))))
                             (begin
                               (add-net biq (a1) 1)
                               (and (> len 2)
                                    (define-a. biq (a1) (a2))))))

                 ;; (reg bit-width name def)
                 ;; (reg bit-width name)
                 ;; (reg name def)
                 ;; (reg name)
                 ;;
                 ((reg)  (if (number? (a1))
                             (begin
                               (add-flop biq (a2) (a1))
                               (and (> len 3)
                                    (define-a. biq (a2) (a3))))
                             (begin
                               (add-flop biq (a1) 1)
                               (and (> len 2)
                                    (define-a. biq (a1) (a2))))))

                 ;; (a. wire def)
                 ;;
                 ((a.)   (define-a. biq (cleaned (a1)) (a2)))

                 ;; (n. flop def)
                 ;;
                 ((n.)   (define-n. biq (a1) (a2)))

                 ;; (w/ blk-name iref-name port-list)
                 ;;
                 ((w/)   (add-iref biq (a1) (a2) (r2)))

                 ;; default
                 ;;
                 (else
                  (dbg DBGGEN (fln "[-d] sim eval: ~S" form))
                  (error "badness:" form)))))

           ;; Do it!
           ;;
           (let loop ((form (read p)))
             (and (eof-object? form)
                  (return #f))
             (scan-dispatch form)
             (loop (read p))))))))
  (log-event 'done 'adding))

;;; (thud scan) ends here
