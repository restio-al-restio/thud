;;; inf-thud.el --- an inferior thud mode

;; Copyright (C) 1999-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file provides inferior thud mode (consisting of two submodes,
;; "THUD:Emacs" and "THUD:Comint"), and is documented in the THUD Manual.

;;; Code:

(require 'cl)
(require 'comint)

;;;---------------------------------------------------------------------------
;;; Variables

(defvar inf-thud-program "thud --face emacs"
  "*Program name for invoking an inferior THUD.")

(defvar inf-thud-buffer nil
  "Buffer where inf-thud mode commands go.")

(defvar inf-thud-this-command nil
  "Current command sent by `inf-thud-send-string'.")

(defvar inf-thud-mode-terse-cmd-face-map nil
  "Keymap for inf-thud mode (terse-cmd face).")

(defvar inf-thud-mode-emacs-face-map nil
  "Keymap for inf-thud mode (Emacs face).")

;;;---------------------------------------------------------------------------
;;; Support functions

(defun inf-thud-relative-path (path)
  (require 'dired)
  (condition-case error
      (dired-make-relative path)
    (error path)))

(defun inf-thud-send-string (string &optional pause)
  (interactive "sScheme Expression: ")
  (insert (setq inf-thud-this-command string))
  (comint-send-input)
  (when pause (sit-for pause)))

;; These next two sexps are highly experimental.
;; At this time, we kludge a call to `terse-cmd-face'.
;; Should probably factor it into `interpret-terse-cmd'.

(setq inf-thud-send-terse-cmd           ; yay lisp-N !
      '(define (@terse-cmd string)
         (let ((orig-prompt thud-prompt)
               (orig-inport (current-input-port)))
           (catch (->bool ->bool)       ; keep elisp reader happy
             (lambda ()
               (set! thud-prompt "")
               (call-with-input-string
                (string-append string "\nq\n")
                (lambda (in-port)
                  (set-current-input-port in-port)
                  (terse-cmd-face)
                  (echo "mysterious weirdness, please report!"))))
             (lambda args
               (set! thud-prompt orig-prompt)
               (set-current-input-port orig-inport)
               *unspecified*)))))

(defun inf-thud-maybe-kludge-terse-cmd () ; yuk!
  (unless (get 'inf-thud 'inf-thud-maybe-kludge-terse-cmd)
    (message "Doing one-time terse-cmd kludge... hold on...")
    (let ((p (point)))
      (dolist (s (list "(define-module (thud face))"
                       "(export @terse-cmd)"
                       (format "%S" inf-thud-send-terse-cmd)
                       "(define-module (thud user))"))
        (inf-thud-send-string s 1.5))
      (delete-region p (point-max)))
    (message "")
    (put 'inf-thud 'inf-thud-maybe-kludge-terse-cmd t)))

(defun inf-thud-send-terse-cmd (string &optional pause)
  (interactive "sTerse Command: ")
  (inf-thud-maybe-kludge-terse-cmd)     ; yuk!
  (inf-thud-send-string (format "(@terse-cmd %S)" string) pause))

;; These next two macros are admittedly a bit on the lame side.  If someone
;; comes up w/ a better way, I'll be glad to use their implementation.

(defmacro inf-thud-synchronously (out-filter &rest body)
  `(let ((cur-map (current-local-map))
         (wait-lock t)
         (inf-thud-synchronous-acc nil)
         (comint-preoutput-filter-functions
          (list (lambda (string)
                  (cond ((string-match "<experimental-emacs>" string)
                         (setq wait-lock nil)
                         string)
                        (t
                         (funcall ,out-filter string)))))))
     (use-local-map (make-keymap))
     (suppress-keymap (current-local-map))
     ,@body
     (while wait-lock (sit-for 0.5))    ; oh my
     (use-local-map cur-map)
     inf-thud-synchronous-acc))

(defmacro inf-thud-output-to-list (&rest body)
  `(split-string
    (inf-thud-synchronously
     (lambda (string)
       (setq inf-thud-synchronous-acc
             (concat inf-thud-synchronous-acc string))
       "")
     ,@body)))

(defun inf-thud-scope ()
  (inf-thud-maybe-kludge-terse-cmd)     ; yuk!
  (or (get 'inf-thud 'scope)
      (let ((scope (car (inf-thud-output-to-list
                         (inf-thud-send-string "scope")))))
        (put 'inf-thud 'scope scope)
        scope)))

(defun inf-thud-scopes ()
  (inf-thud-maybe-kludge-terse-cmd)     ; yuk!
  (or (get 'inf-thud 'scopes)
      (let ((scopes (inf-thud-output-to-list
                     (inf-thud-send-terse-cmd "info scopes"))))
        (put 'inf-thud 'scopes scopes)
        scopes)))

;;;---------------------------------------------------------------------------
;;; Commands

(defun inf-thud-execute-user-script-cmd (script)
  (interactive "fScript to execute: ")
  (inf-thud-send-string (format "(thud-execute-user-script %S)"
                                (inf-thud-relative-path
                                 (expand-file-name script)))))

(defun inf-thud-add-cmd (file)
  (interactive "fFile to add: ")
  (inf-thud-send-string (format "(add %S)"
                                (inf-thud-relative-path
                                 (expand-file-name file)))))

(defun inf-thud-prepare-to-simulate-cmd ()
  (interactive)
  (inf-thud-send-string "(prepare-to-simulate)"))

(defun inf-thud-next-cmd (count)
  (interactive "p")
  (inf-thud-send-string (format "(next %d)" count)))

(defun inf-thud-reset-cmd ()
  (interactive)
  (when (y-or-n-p "Sure you want to reset? ")
    (put 'inf-thud 'scope nil)
    (put 'inf-thud 'scopes nil)
    (inf-thud-send-string "(reset-internals)")))

(defun inf-thud-quit-cmd ()
  (interactive)
  (inf-thud-send-string "(quit)"))

(defun inf-thud-print-cmd (expression)
  (interactive "sExpression: ")
  (inf-thud-send-string (format "(eval-print %S)" expression)))

(defun inf-thud-deposit-cmd (details)
  (interactive "sDetails: ")
  (inf-thud-send-string (format "(deposit %S)" details)))

(defun inf-thud-toggle-mon-cmd ()
  (interactive)
  (put 'inf-thud-toggle-mon-cmd 'inf-thud-toggle-mon-cmd
       (not (get 'inf-thud-toggle-mon-cmd 'inf-thud-toggle-mon-cmd)))
  (inf-thud-send-string
   (format "(%s-hook! post-cycle-hook display-scheduled-values)"
           (if (get 'inf-thud-toggle-mon-cmd 'inf-thud-toggle-mon-cmd)
               "add"
             "remove"))))

(defun inf-thud-compile-to-cmd (file)
  (interactive "FCompile to: ")
  (inf-thud-send-string (format "(compile-to %S)" (expand-file-name file))))

(defun inf-thud-gcmd ()
  (interactive)
  (let* ((key (this-command-keys))
         (keylist (listify-key-sequence key)))
    (setq unread-command-events (append keylist unread-command-events)))
  (call-interactively 'inf-thud-send-terse-cmd))

(defun inf-thud-set-scope (new-scope)
  (interactive (list (completing-read "New Scope: "
                                      (mapcar 'list
                                              (cons "/" (inf-thud-scopes)))
                                      (lambda (x) t) ; hmm
                                      t
                                      (inf-thud-scope))))
  (put 'inf-thud 'scope new-scope)
  (inf-thud-send-string (format "(set-scope! %S)" new-scope)))

;;---------------------------------------------------------------------------
;;; Different modes

(defun inf-thud-mode-emacs-face ()
  "Major mode for dealing w/ a THUD process (that uses \"--face emacs\")."
  (setq major-mode 'inf-thud-mode-emacs-face
        mode-name "THUD:Emacs")
  (put 'inf-thud-toggle-mon-cmd 'inf-thud-toggle-mon-cmd nil)
  (use-local-map inf-thud-mode-emacs-face-map))

(defun inf-thud-mode ()
  "Major mode for dealin w/ a THUD process (not using \"--face emacs\")."
  (setq major-mode 'inf-thud-mode
        mode-name "THUD:Comint")
  (use-local-map inf-thud-mode-terse-cmd-face-map))

;;;---------------------------------------------------------------------------
;;; Entry point

(defun inf-thud (cmd)
  "Run an inferior THUD process.
With argument, allows you to edit the command line (default is value
of `inf-thud-program').  Emacs enters either `inf-thud-mode'
or `inf-thud-mode-emacs-face' based on whether or not the command
includes \"--face emacs\".  See those functions for more info."
  (interactive (list (if current-prefix-arg
                         (read-string "Run THUD: " inf-thud-program)
                       inf-thud-program)))
  (let ((buf-name "*inf-thud*"))
    (unless (comint-check-proc buf-name)
      (let ((cmdlist (split-string cmd)))
        (set-buffer (apply (function make-comint)
                           "inf-thud" (car cmdlist) nil (cdr cmdlist)))
        (setq inf-thud-mode-terse-cmd-face-map (current-local-map))
        (put 'inf-thud 'scope nil)
        (put 'inf-thud 'scopes nil)
        (put 'inf-thud 'inf-thud-maybe-kludge-terse-cmd nil)
        (if (string-match "--face emacs" cmd)
            (inf-thud-mode-emacs-face)
          (inf-thud-mode))))
    (setq inf-thud-buffer buf-name)
    (switch-to-buffer buf-name)))

;;;---------------------------------------------------------------------------
;;; Load-time actions

;; This is arguably the wrong way to go about things.  Ideally we would grok
;; `terse-cmd-table' and automatically build the correct keymaps.

(unless inf-thud-mode-emacs-face-map
  (suppress-keymap (setq inf-thud-mode-emacs-face-map (make-keymap)))
  (mapcar (lambda (m)
            (define-key inf-thud-mode-emacs-face-map (car m) (cdr m)))
          '(;; standard
            (","        .       inf-thud-send-string)
            ("!"        .       shell-command)
            (":"        .       inf-thud-send-terse-cmd)
            ("x"        .       inf-thud-execute-user-script-cmd)
            ("a"        .       inf-thud-add-cmd)
            ("l"        .       inf-thud-prepare-to-simulate-cmd)
            ("n"        .       inf-thud-next-cmd)
            ("\C-m"     .       inf-thud-next-cmd)
            ("R"        .       inf-thud-reset-cmd)
            ("q"        .       inf-thud-quit-cmd)
            ("p"        .       inf-thud-print-cmd)
            ("d"        .       inf-thud-deposit-cmd)
            ("i"        .       inf-thud-gcmd)
            ;; bonus
            ("h"        .       inf-thud-gcmd)
            ("?"        .       describe-mode)
            ("m"        .       inf-thud-toggle-mon-cmd)
            ("c"        .       inf-thud-compile-to-cmd)
            ("\C-d"     .       inf-thud-quit-cmd)))
  (let ((s-map (make-keymap)))
    (suppress-keymap s-map)
    (define-key s-map "e" 'inf-thud-gcmd)       ; set
    (define-key s-map "h" 'inf-thud-gcmd)       ; show
    (define-key s-map "c" 'inf-thud-set-scope)
    (define-key inf-thud-mode-emacs-face-map "s" s-map)))

(provide 'inf-thud)

;;; inf-thud.el ends here
