;;; (thud sim) --- Simulation object creation and manipulation

;; Copyright (C) 1998-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; The simulation object is currently implemented as the procedure
;; `one-cycle', defined in the `(thud runtime)' module.  This is
;; called repeatedly by `next'.

;;; Code:

(define-module (thud sim)
  #:export (settle
            update-runtime-module
            SIM:write-to-file)
  #:autoload (ice-9 pretty-print) (pretty-print)
  #:use-module ((thud config-info) #:select (%thud-config-info))
  #:use-module ((thud types) #:select (vn-enof vn-bw eform flop?))
  #:use-module ((thud state) #:select (session k))
  #:use-module ((thud com) #:select (human-readable-time-stamp
                                     fln dbg vn-val))
  #:use-module ((thud runtime) #:select (eval-in-the-thud-runtime-module
                                         set-current-cycle!
                                         cc)))

;;;---------------------------------------------------------------------------
;;; Settle, let the machine take a de-heap breath

(define (settle)
  (and=> (session #:vcd-mangle) (lambda (m) (m #:prep!)))
  (set! (session #:TODO) (map eform (session #:duty)))
  (gc))

;;;---------------------------------------------------------------------------
;;; Creating the simulation object

(define remap-rep
  (let ((op-map (k #:op-map)))
    (lambda (x)
      (if (list? x)
          (map remap-rep x)
          (let ((try (assq x op-map)))
            (if try (cdr try) x))))))

(define (remap-eform ef)
  (dbg DBGGEN
       (fln "remapping eform: ~S" ef))
  (let ((res (list (car ef) (cadr ef) (remap-rep (caddr ef))))) ; hmm brittle
    (dbg DBGGEN
         (fln "new eform: ~S" res))
    res))

(define (annotated-settled-todo-list)
  `(,@(list (if (hook? (session #:bef-cycle))
                '(run-hook (session #:bef-cycle))
                '()))
    ,@(map (lambda (flop)
             `(set! ,flop (fnext ',flop)))
           (map car (session list #:flops)))
    ,@(map (lambda (ef)
             (if (caddr ef)
                 (remap-eform ef)
                 '()))
           (session #:TODO))
    ,@(list (if (hook? (session #:aft-cycle))
                '(run-hook (session #:aft-cycle))
                '()))
    (set-current-cycle! (1+ (cc)))))

(define (update-runtime-module)
  (for-each (lambda (sig-var)           ; was: expose-vob
              (eval-in-the-thud-runtime-module
               `(define-alias ',(car sig-var) ,(cdr sig-var))))
            (session list #:vob))
  (eval-in-the-thud-runtime-module
   `(define (one-cycle)
      ,@(annotated-settled-todo-list))))

;;;---------------------------------------------------------------------------
;;; Compilation support

(define (display-sim-object)
  (define (pp1 form)
    (pretty-print form #:escape-strings? #t))

  (define (pp . forms)
    (for-each pp1 forms))

  (define (summarize-pool! nickname pool)
    (let ((all (session list pool)))
      (fln ";; ~S ~A:" (length all) nickname)
      (for-each (lambda (ent)
                  (fln ";; - ~S" (car ent)))
                all)))

  ;; do it!
  (fln "#!/bin/sh")
  (fln "exec ${GUILE-guile} -L ~S -s $0 \"$@\" # -*- scheme -*-"
       (cond ((%thud-config-info 'abs-top-builddir)
              => (lambda (dir)
                   (in-vicinity dir "src")))
             (else (%thud-config-info 'pkglibdir))))
  (fln "!#")
  (fln ";;; status:")
  (fln ";; generated: ~A" (human-readable-time-stamp))
  (summarize-pool!    "blocks" #:blocks)
  (summarize-pool! "instances" #:instances)

  (fln "\n;;; configuration:\n")
  (pp '(use-modules (thud runtime))
      '(define-module (thud runtime)))

  (fln "\n;;; initializations:\n")
  (let ((all-flops '()))
    (pp1 `(define *peek-thunks* (make-hash-table ,(length (session #:TODO)))))
    (for-each (lambda (ef)
                (if (memq (car ef) '(set! fnext!))
                    (let ((sym (cadr ef)))
                      (pp `(define-sig ,(vn-bw sym) ,sym))
                      (and (flop? (vn-enof sym))
                           (set! all-flops (cons sym all-flops))))
                    (fln ";;;;;;;;; hmmm: ~S" ef)))
              (session #:TODO))
    (pp1 '(set! node-list (settled-node-list)))
    (for-each (lambda (pair)
                (pp1 `(defalias ,(car pair) ,(cdr pair))))
              (session list #:aliases))
    (pp `(set! all-flops ',all-flops)
        '(set! (session #:aft-cycle) (make-hook)))
    (map (lambda (h)
           (pp1 `(add-hook! (session #:aft-cycle)
                            ,(if (procedure? h)
                                 (or (procedure-name h)
                                     (procedure-source h))
                                 h))))
         (if (hook? (session #:aft-cycle))
             (hook->list (session #:aft-cycle))
             '()))
    (and (session #:cmax)
         (pp1 `(add-hook! (session #:aft-cycle)
                          (lambda ()
                            (and (= ,(session #:cmax) (cc))
                                 (throw 'done-sim #t))))))

    (fln "\n;;; one-cycle:\n")
    (pp1 `(define (one-cycle)
            ,@(if (null? all-flops)
                  (list #t)
                  (map (lambda (flop)
                         `(set! ,flop (fnext ',flop)))
                       all-flops))
            ,@(map remap-eform (session #:TODO))))

    (fln "\n;;; display-scheduled-values:\n")
    (pp1 (let ((nodes (map cadr (session #:TODO))))
           `(define (display-scheduled-values)
              (fln ,(apply string-append (make-list (length nodes) " ~A"))
                   ,@(map (lambda (node)
                            `(n->fw-bstr ,(vn-bw node) ,node))
                          nodes)))))

    (fln "\n;;; do it!\n")
    (pp1 '(SIM:run-forever))))

(define (SIM:write-to-file file)
  (call-with-output-file file
    (lambda (outfile-port)
      (with-output-to-port outfile-port display-sim-object)))
  (chmod file #o755))

;;; (thud sim) ends here
