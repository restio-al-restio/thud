;;; (thud types) --- Types used by THUD

;; Copyright (C) 1998-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file defines types used by THUD, including proecedures to access,
;; mutate and print each type.
;;
;; Types include: enof, iref, blk, vn, instance, dn, en.  There is a lot of
;; cross-linking.  Nets, flops and ports are `enof' objects; instantiations
;; are assocations between templates (`blk' objects) and actual storage space
;; (`vn' objects); dependencies are captured in `dn' objects, and are used to
;; create lists of execution forms (`ef') to evaluate.  Here is a picture:
;;
;;      +-> [enof] {bw rep deps pdir flop}      [iref] {blk box maps}
;;      |      ^                                 ^
;;      |      |                                 |
;;      [vn]   +------------------------------- [blk]
;;      ^  ^                                     ^
;;      |  |                                     |
;;      |  +-------------+      +----------------+
;;      |                |      |
;;      [dn] ---------> [instance]
;;      ^
;;      |
;;      [en] {ef}
;;
;; In the picture, A->B indicates that A uses B or some part of B (usually by
;; reference).  Curly braces enclose some of the named fields.  In addition to
;; the above types, some of the containers below use a standard dictionary
;; type conforming to conventions given in the Guile manual.

;;; Code:

(define-module (thud types)
  #:export (make-dic                    ; dictionaries
            d-ref d-set! d-remove!
            d->list d-alloc d-size d-ls

            make-enof                   ; enof
            make-port make-net make-flop enof? PORT? flop? net? in-port?
            enof-bw enof-rep enof-deps enof-pdir enof-flop pdir
            enof-rep-set! enof-deps-set! enof-pdir-set! enof-flop-set!

            make-iref                   ; iref
            iref? iref-blk iref-box iref-maps
            iref-blk-set! iref-box-set! iref-maps-set!

            make-blk                    ; blk
            make-incomplete-blk blk?
            blk-ports blk-nets blk-flops blk-irefs blk-ref?
            blk-ports-set! blk-nets-set!
            blk-flops-set! blk-irefs-set! blk-ref?-set!

            make-vn                     ; vn
            vn-enof vn-rep vn-bw vn-deps

            make-instance               ; instance
            instance? instance-blk instance-values
            instance-blk-set! instance-values-set!

            make-dn                     ; dn
            dn? dn-obj dn-back dn-inm display-dn

            make-en                     ; en
            en? eform en-dn en-rep en-enof))


;;;---------------------------------------------------------------------------
;;; Dictionaries
;;;
;;; Some container structures below follow the dictionary convention outlined
;;; in the Guile reference manual.  We extend the convention by including a
;;; listifying procedure, an iteration procedure and (for hash-table tuning)
;;; alloc and size procedures.

; (Never used, here for completeness.)
;
; ;; Alist support.
; ;;
; (define assq-for-each for-each)
; (define assq-href     assoc)
; (define assq-alloc    length)
; (define assq-size     length)

;; Hash table support.  (Note: hashq-alloc is not really correct.)
;;
(define (ht->list ht)         (let ((acc '()))
                                (hash-for-each (lambda (k v)
                                                 (set! acc (acons k v acc)))
                                               ht)
                                acc))
(define (hashq-alloc ht)      (car (array-dimensions ht))) ; FIXME: wrong
(define (hashq-size ht)       (length (ht->list ht)))

;; Dictionary interface.
;;
;; We use hash tables.
;;
(define (make-dic . sz) (make-hash-table (if (null? sz) 431 (car sz))))
(define d-ref           hashq-ref)
(define d-set!          hashq-set!)
(define d-remove!       hashq-remove!)
(define d->list         ht->list)
(define d-alloc         hashq-alloc)
(define d-size          hashq-size)

(define (d-ls d)
  (let ((count 1))
    (for-each (lambda (nm-ignore)
                (display count)
                (display " ")
                (display (car nm-ignore))
                (newline)
                (set! count (1+ count)))
              (d->list d))))


;;;---------------------------------------------------------------------------
;;; Record type `enof' (evaluatable net or flop)
;;;
;;; THUD targets synchronous designs, so two main object types are nets and
;;; flops.  Here is a picture:
;;;
;;;             +-------+
;;;  ---NET---->| FLOP  |---->
;;;          (1)|       |  (2)
;;;             |>rst   |          (todo: implement rst, env, inv)
;;;             |>en    |
;;;             |>inv   |
;;;             +-------+
;;;
;;; Nets are arbitary logic to be evaluated during the cycle.  Flops are
;;; places where these values can be stored for the next cycle.  Note that
;;; there is no clock for the flop; THUD supplies this automatically.
;;;
;;; We call the "value" of a net the result at (1), and the "value" of a flop
;;; the result at (2).  Thus, during a "cycle", first the nets are evaluated
;;; then the flops.
;;;
;;; Recognizing that both have similar internal structure, we unify using one
;;; record type: `enof', to make things easy.  Ports (see data type `blk') are
;;; similar enough to implement using `enof', also.

(define enof
  (make-record-type 'enof               ; evaluatable net or flop
                    '(bw                ; bit width
                      rep               ; working representation
                      deps              ; dependencies
                      pdir              ; direction (in/out) if a port
                      flop              ; #t if a flop
                      )
                    (lambda (enof p)
                      (with-output-to-port p
                        (lambda ()
                          (if (> (enof-bw enof) 1)
                              (begin (display (enof-bw enof)) (display "/")))
                          (cond ((enof-pdir enof) (display (enof-pdir enof)))
                                ((enof-flop enof) (display 'flop))
                                (else             (display 'wire)))
                          (display " ")
                          (display (enof-deps enof)))))))

(define make-enof  (record-constructor enof '(bw)))
(define make-port  (record-constructor enof '(bw pdir)))
(define make-net   make-enof)
(define mk-flop    make-enof)
(define (make-flop bw)
  (letrec ((new (mk-flop bw)))
    (enof-flop-set! new #t)
    new))

(define enof?     (record-predicate enof))
(define (PORT? x) (and (enof? x) (enof-pdir x)))
(define (flop? x) (and (enof? x) (enof-flop x)))
(define (net?  x) (and (enof? x) (not (PORT? x)) (not (flop? x))))

(define (in-port?  x) (and (PORT? x) (eq? 'in  (enof-pdir x))))

(define enof-bw   (record-accessor enof 'bw))
(define enof-rep  (record-accessor enof 'rep))
(define enof-deps (record-accessor enof 'deps))
(define enof-pdir (record-accessor enof 'pdir))
(define enof-flop (record-accessor enof 'flop))

(define pdir enof-pdir)

(define enof-rep-set!  (record-modifier enof 'rep))
(define enof-deps-set! (record-modifier enof 'deps))
(define enof-pdir-set! (record-modifier enof 'pdir))
(define enof-flop-set! (record-modifier enof 'flop))


;;;---------------------------------------------------------------------------
;;; Record type `iref'
;;;
;;; An iref is an instance reference, done so by naming a template blk and
;;; providing proper input and output maps.  A proper map names the port and
;;; wire used.  Irefs also know about the blk that instantiated them.

(define iref
  (make-record-type 'iref               ; instance reference
                    '(blk               ; kind of instance
                      box               ; that which contains this
                      maps              ; dictionary of port maps
                      )
                    (lambda (iref p)
                      (with-output-to-port p
                        (lambda ()
                          (display (d-size (iref-maps iref)))
                          (if (iref-blk iref)
                              (or (blk-ports (iref-blk iref))
                                  (display "!ports"))
                              (display "!blk"))
                          (or (iref-box iref) (display "!box")))))))

(define mk-iref (record-constructor iref '(blk box)))
(define (make-iref blk box maps)
  (letrec ((new (mk-iref blk box)))
    (iref-maps-set! new (make-dic))
    (for-each (lambda (map)             ; (port . wire)
                (d-set! (iref-maps new) (car map) (cdr map)))
              maps)
    new))

(define iref?
  (record-predicate iref))

(define iref-blk  (record-accessor iref 'blk))
(define iref-box  (record-accessor iref 'box))
(define iref-maps (record-accessor iref 'maps))

(define iref-blk-set!  (record-modifier iref 'blk))
(define iref-box-set!  (record-modifier iref 'box))
(define iref-maps-set! (record-modifier iref 'maps))


;;;---------------------------------------------------------------------------
;;; Record type `blk'
;;;
;;; A blk is an aggregation and encapsulation of ports, nets, flops and irefs.
;;; It has input and output ports (currently inout ports are not supported),
;;; which map onto nets internally, but may be named separately.  A blk has a
;;; name that is used when instantiating it to create an iref.  A blk may
;;; contain irefs, but not other blks.  Blks know if they are not at the
;;; top-level (iref'ed by other blks).
;;;
;;; A `blk' arises from one of two places: explicit declaration in a .th file,
;;; and implicit requirement when creating an iref.  In the latter case, the
;;; actual port list is unknown and the blk is called "incomplete".  At some
;;; point, a blk must become "complete" to be used.

(define blk
  (make-record-type 'blk                ; akin to Verilog "module"
                    '(ports             ; dictionary, or #f => incomplete
                      nets              ; dictionary
                      flops             ; dictionary
                      irefs             ; dictionary
                      ref?              ; #t if blk has been iref'ed
                      )
                    (lambda (blk p)
                      (with-output-to-port p
                        (lambda ()
                          (define (name/obj nm-obj)
                            (display (car nm-obj))
                            (display " = ")
                            (display (cdr nm-obj))
                            (newline))
                          (and (blk-ports blk)
                               (begin
                                 (simple-format #t "** ports:\n")
                                 (for-each name/obj
                                           (d->list (blk-ports blk)))))
                          (and (blk-nets blk)
                               (begin
                                 (simple-format #t "** nets:\n")
                                 (for-each name/obj
                                           (d->list (blk-nets  blk)))))
                          (and (blk-flops blk)
                               (begin
                                 (simple-format #t "** flops:\n")
                                 (for-each name/obj
                                           (d->list (blk-flops blk)))))
                          (and (blk-irefs blk)
                               (begin
                                 (simple-format #t "** irefs:\n")
                                 (for-each name/obj
                                           (d->list (blk-irefs blk)))))
                          (display "** ref?: ")
                          (display (blk-ref? blk))
                          (newline))))))

(define mk-blk (record-constructor blk '()))
(define (make-blk)
  (letrec ((new (mk-blk)))
    (blk-ports-set! new (make-dic))
    (blk-nets-set!  new (make-dic))
    (blk-flops-set! new (make-dic))
    (blk-irefs-set! new (make-dic))
    (blk-ref?-set!  new #f)
    new))

(define (make-incomplete-blk)   ; ports set to #f
  (letrec ((new (make-blk)))
    (blk-ports-set! new #f)
    new))

(define blk?
  (record-predicate blk))

(define blk-ports (record-accessor blk 'ports))
(define blk-nets  (record-accessor blk 'nets))
(define blk-flops (record-accessor blk 'flops))
(define blk-irefs (record-accessor blk 'irefs))
(define blk-ref?  (record-accessor blk 'ref?))

(define blk-ports-set! (record-modifier blk 'ports))
(define blk-nets-set!  (record-modifier blk 'nets))
(define blk-flops-set! (record-modifier blk 'flops))
(define blk-irefs-set! (record-modifier blk 'irefs))
(define blk-ref?-set!  (record-modifier blk 'ref?))


;;;---------------------------------------------------------------------------
;;; Type `vn' (value node)
;;;
;;; A value node is a symbol.  It's value is the structure "variable", which
;;; has a name field to hold the value node's name, as well as a binding,
;;; which is the actual value used by `set!' and referenced when executing.
;;;
;;; The procedure `make-vn' takes a symbol, and enof and rep pointers.  The
;;; latter two are set as properties, and 'bw' (bit-width) copied from enof.

(define (make-vn-property . opts)
  ;; This is basically `make-object-property' from Guile.  However, we have
  ;; plans for it: it needs to be able to encapsulate a "current context"
  ;; (which will probably affect performance, unfortunately).
  (let ((dv (and (not (null? opts)) (car opts)))
        (ht (make-weak-key-hash-table 3)))
    (make-procedure-with-setter
     (lambda (obj)     (hashq-ref  ht obj dv))
     (lambda (obj val) (hashq-set! ht obj val)))))

(define vn-enof (make-vn-property))
(define vn-rep  (make-vn-property))
(define vn-bw   (make-vn-property))
(define vn-deps (make-vn-property))

(define (make-vn name enof rep)
  (set! (vn-enof name) enof)
  (set! (vn-bw   name) (enof-bw enof))
  (set! (vn-rep  name) rep)
  name)


;;;---------------------------------------------------------------------------
;;; Record type `instance'
;;;
;;; The instance record associates a blk w/ a set of values.  The value field
;;; is a dictionary of value nodes (type `vn').  These values are then shared
;;; with the values obarray.

(define instance
  (make-record-type 'instance           ; instantiation of a blk
                    '(blk               ; the blk
                      values)           ; dictionary of values
                    (lambda (instance p)
                      (display (instance-blk instance)))))

(define mk-instance (record-constructor instance '(blk)))
(define (make-instance blk)
  (letrec ((new (mk-instance blk)))
    (instance-values-set! new (make-dic))
    new))

(define instance?
  (record-predicate instance))

(define instance-blk    (record-accessor instance 'blk))
(define instance-values (record-accessor instance 'values))

(define instance-blk-set!    (record-modifier instance 'blk))
(define instance-values-set! (record-modifier instance 'values))


;;;---------------------------------------------------------------------------
;;; Record type `dn' (dependency node)
;;;
;;; Dependency nodes know that an object is dependent on a list of other
;;; objects.  Sort of warty, but off to the side, we keep the instance name of
;;; this node for -- no doubt -- ugly hacks later.  (todo: move field `inm' to
;;; `vn' or better yet, off it.)

(define dn
  (make-record-type 'dn                 ; dependency node
                    '(obj               ; the object
                      back              ; list of what it depends on
                      inm               ; instance name for later deref
                      )))

(define make-dn (record-constructor dn '(obj back inm)))
(define dn?     (record-predicate dn))

(define dn-obj  (record-accessor dn 'obj))
(define dn-back (record-accessor dn 'back))
(define dn-inm  (record-accessor dn 'inm))

(define (display-dn n)
  (display " ")
  (display (dn-obj n))
  (display (dn-back n)))


;;;---------------------------------------------------------------------------
;;; Record type `en' (execution node)
;;;
;;; Execution nodes have passing interest in a dependency node, but ultimately
;;; their payload is the execution form.  Execution forms currently have one
;;; of two formats.  Here is a picture:
;;;
;;;     (set!   VN EXP)
;;;     (fnext! VN EXP)
;;;
;;; The "todo list" is made of execution nodes, while "settled todo list" is
;;; made of execution forms.

(define en
  (make-record-type 'en                 ; execution node
                    '(ef                ; the executable form
                      dn                ; dependency node that creates it
                      )))

(define make-en (record-constructor en '(ef dn)))
(define en?     (record-predicate en))

(define eform (record-accessor en 'ef))
(define en-dn (record-accessor en 'dn))

(define (en-rep  en) (vn-rep  (dn-obj (en-dn en))))
(define (en-enof en) (vn-enof (dn-obj (en-dn en)))) ; hmmm

;;; (thud types) ends here
