;;; (thud svc0) --- Innermost layer of THUD services.

;; Copyright (C) 1998-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(define-module (thud svc0)
  #:export (reset-internals             ; data structures
            display-todo-list-reps
            display-scheduled-values
            display-vob
            display-blks
            display-blks-summary
            display-instances-summary
            display-scopes

            add                         ; adding
            add-only

            prepare-to-simulate         ; simulation
            next

            set-dump-file!              ; dumping
            dumpvars

            compile-to                  ; compilation

            format-error-args           ; eval and print
            eval-print

            deposit                     ; deposit

            cd                          ; misc
            display-thud-options
            display-system-info
            thud-exit)
  #:use-module ((ice-9 and-let-star) #:select (and-let*))
  #:use-module ((thud config-info) #:select (%thud-config-info))
  #:use-module ((thud types) #:select (d->list
                                       en-dn dn-obj
                                       vn-bw vn-rep
                                       blk-ports blk-nets
                                       blk-flops blk-irefs
                                       instance-blk))
  #:use-module ((thud state) #:select (k session global-state-reset!))
  #:use-module ((thud com) #:select (dbg
                                     die
                                     maybe-say
                                     fs fln
                                     n->fw-bstr
                                     vn-val))
  #:use-module ((thud evlog) #:select (log-event-verbosely
                                       log-event))
  #:use-module ((thud io) #:select (make-vcd-mangler))
  #:use-module ((thud scan) #:select (add-th-file
                                      populate-instance-pool))
  #:use-module ((thud sched) #:select (set-schedules!))
  #:use-module ((thud sim) #:select (settle
                                     update-runtime-module
                                     SIM:write-to-file))
  #:use-module ((thud runtime) #:select (set-current-cycle!
                                         cc
                                         eval-in-the-thud-runtime-module)))

;;;---------------------------------------------------------------------------
;;; Link-phase driver
;;;

;; Populate instance pool, form and optimize schedule, and
;; settle data structures in preparation for simulation.
;;
(define (determine-settled-todo-list)
  (log-event-verbosely 'instantiating (list (length (session list #:blocks))
                                            'blks))
  (populate-instance-pool)
  (log-event 'done 'instantiating)
  (log-event-verbosely 'linking (list (length (session list #:instances))
                                      'instances))
  (set-schedules! session)
  (settle)
  (update-runtime-module)
  (log-event 'done 'linking))

;;;---------------------------------------------------------------------------
;;; Internal data structures
;;;

(define (reset-internals)
  "Make everything as if rebooting THUD.
Also, record this in the event log."
  (global-state-reset!)
  (and=> (session #:vcd-mangle) (lambda (m) (m #:reset!)))
  (set-current-cycle! 0)
  (log-event 'internals 'reset))

(define (display-todo-list-reps . msg)
  "Display todo list representations.  Display optional arg MSG first.
Each line displayed shows a sequence number, the signal bit-width, the signal
name (fully expanded) and its evaluation representation, which may have become
expanded during linking.

The order of the display reflects exactly the order of evaluation during
simulation.  See also `display-scheduled-values' for a horizontal view of the
same data.

This procedure obeys the `scope' variable, q.v."
  (maybe-say msg)
  (let ((re (make-regexp (string-append "^" (session #:scope)))))
    (let loop ((idx 0)
               (ls (session #:duty)))
      (if (not (pair? ls))
          (newline)
          (let* ((dn (en-dn (car ls)))
                 (vn (dn-obj dn)))
            (and (regexp-exec re (symbol->string vn))
                 (fln "~S ~S\t~S ~S" idx (vn-bw vn) (dn-obj dn) (vn-rep vn)))
            (loop (+ 1 idx) (cdr ls)))))))

(define (display-scheduled-values . msg)
  "Display signal values in order.  Display optional arg MSG first.
Each column shows the bit values of a signal, either 0, 1 or x.

The left-to-right order of the display reflects exactly the order of
evaluation during simulation.  See also `display-todo-list-reps' for a
vertical view of the same data.

This procedure obeys the `scope' variable, q.v."
  (maybe-say msg)
  (or (session #:TODO)
      (die "No `(session #:TODO)' to check."))
  (let ((re (make-regexp (string-append "^" (session #:scope)))))
    (for-each (lambda (ef)
                (display " ")
                (let* ((vn  (cadr ef))
                       (bw  (vn-bw vn))
                       (val (vn-val vn)))
                  (and (regexp-exec re (symbol->string vn))
                       (display (n->fw-bstr bw val)))))
              (session #:TODO)))
  (newline))

(define (display-vob . msg)
  "Display the values obarray.  Display optional arg MSG first.
Each line shows bit-width, value (in boolean) and signal name.

This procedure obeys the `scope' variable, q.v."
  (maybe-say msg)
  (let ((re (make-regexp (string-append "^" (session #:scope)))))
    (for-each (lambda (vn-var)
                (and (regexp-exec re (symbol->string (car vn-var)))
                     (let* ((vn  (car vn-var))
                            (var (cdr vn-var))
                            (bw  (vn-bw vn)))
                       (fln "~A ~A~A~A"
                            bw (n->fw-bstr bw (variable-ref var))
                            (if (> bw 5) "\t" "\t\t")
                            vn))))
              (session list #:vob))))

(define (display-blks . msg)
  "Display the block pool.  Display optional arg MSG first.
The block name is displayed then the rest of the block is shown
in relatively high detail."
  (maybe-say msg)
  (for-each (lambda (name-blk)
              (fln "block: ~S\n~S" (car name-blk) (cdr name-blk)))
            (session list #:blocks)))

(define (display-blks-summary . msg)
  "Display summary of the block pool.  Display optional arg MSG first.
Each line has the format NUM-PORTS NUM-NETS NUM-FLOPS NUM-IREFS BLK-NAME."
  (maybe-say msg)
  (for-each
   (lambda (name-blk)
     (let* ((name    (car name-blk))
            (blk     (cdr name-blk))
            (n-ports (length (d->list (blk-ports blk))))
            (n-nets  (length (d->list (blk-nets  blk))))
            (n-flops (length (d->list (blk-flops blk))))
            (n-irefs (length (d->list (blk-irefs blk)))))
       (fln "~S\t~S\t~S\t~S\t~S" n-ports n-nets n-flops n-irefs name)))
   (session list #:blocks)))

(define (display-instances-summary . msg)
  "Display summary of the instance pool.  Display optional arg MSG first.
The output is unordered, but grouped by blocks."
  (maybe-say msg)
  (let ((group '()))
    (for-each
     (lambda (name-instance)
       (let* ((name     (car name-instance))
              (instance (cdr name-instance))
              (blk      (instance-blk instance))
              (blk-name (let loop ((maybe (session list #:blocks))) ; yuk
                          (if (eq? blk (cdar maybe))
                              (caar maybe)
                              (loop (cdr maybe))))))
         (let ((lookup (assoc blk-name group)))
           (if lookup
               (set-cdr! lookup (cons name (cdr lookup)))
               (set! group (cons (list blk-name name) group))))))
     (session list #:instances))
    (for-each (lambda (blk-group)
                (newline)
                (fln "~A" (car blk-group))
                (for-each (lambda (instance-name)
                            (fln ": ~A" instance-name))
                          (cdr blk-group)))
              group)))

(define (display-scopes . msg)
  "Display scopes (instance names).  Display optional arg MSG first."
  (maybe-say msg)
  (map (lambda (x)
         (fln "~A" (car x)))
       (session list #:instances)))

;;;---------------------------------------------------------------------------
;;; Add one or more files
;;;

(define (add . files)
  "Read in rtl FILES, adding blocks to the `(session #:blocks)'.
Calling `add' schedules re-linking (see `prepare-to-simulate')."
  (for-each add-th-file files)
  (set! need-to-settle? #t))

;; Encapsulate reset-internals, some adds, and prepare-to-simulate.
;;
(define (add-only . files)
  "Reset internals, add FILES, then do `prepare-to-simulate'.
The files are added using `add'."
  (reset-internals)
  (apply add files)
  (prepare-to-simulate))

;;;---------------------------------------------------------------------------
;;; Simulation
;;;

(define need-to-settle? #f)             ; should be documented?

(define (prepare-to-simulate)
  "Possibly do re-linking as a preparation for simulation.
This means that all blocks are instantiated, linked and that the simulation
evaluation schedule is created and optimized.

Upon completion, `(session #:duty)' and `(session #:TODO)' can be expected to
be constant.  Procedures that disturb those structures should \"schedule
re-linking\" by setting the variable `need-to-settle?' to `#t' so that
`prepare-to-simulate' knows to do its work and set the variable back to `#f'."
  (if need-to-settle?
      (begin
        (set-current-cycle! 0)
        (determine-settled-todo-list)
        (set! need-to-settle? #f))))

(define (next . count)
  "Simulate COUNT cycles (defaults to 1 if omitted).
Before simulation, call `prepare-to-simulate' to be safe."
  (prepare-to-simulate)
  (dbg DBGGEN
       (fln "[-d] pre-cycle-hook: ~S" (session #:bef-cycle))
       (fln "[-d] post-cycle-hook: ~S" (session #:aft-cycle))
       (fln "[-d] one-cycle definition: ~S"
            (procedure-source (eval-in-the-thud-runtime-module 'one-cycle))))
  (let ((repeat-count (if (pair? count) (car count) 1)))
    (and (> repeat-count 1)
         (log-event 'simulating `(,repeat-count cycles)))
    (eval-in-the-thud-runtime-module
     `(let loop ((i ,(if (session #:cmax)
                         (min (session #:cmax) repeat-count)
                         repeat-count)))
        (or (zero? i)
            (begin
              (one-cycle)
              (loop (1- i))))))
    (and (> repeat-count 1)
         (log-event 'done 'simulating))))

;;;---------------------------------------------------------------------------
;;; Dumping

(define (set-dump-file! file)
  "Set the dump file to FILE.  Log event.
Calling `set-dump-file!' schedules re-linking (see `prepare-to-simulate')."
  (dbg DBGDMP (fln "[-d] dump-file previously: ~S" (session #:vcd-file)))
  (set! (session #:vcd-file) file)
  (log-event-verbosely 'set-dump-file! file)
  (fln "Dump file now set to: ~S" (session #:vcd-file))
  (set! need-to-settle? #t))

(define (dumpvars . sigs)
  "Register for dumping all SIGS.  If SIGS is omitted, dump all signals.
Calling `dumpvars' schedules re-linking (see `prepare-to-simulate')."
  (let ((m (or (session #:vcd-mangle)
               (set! (session #:vcd-mangle) (make-vcd-mangler vn-val cc)))))
    (m #:register! sigs))
  (set! need-to-settle? #t))

;;;---------------------------------------------------------------------------
;;; Compilation

(define (compile-to file)
  "Compile the current design, writing output to FILE.
The output file can be executed like so: \"guile -s FILE\".  The
filename extension is normally \".thc\", but that is not required."
  (prepare-to-simulate)
  (log-event-verbosely 'writing file)
  (SIM:write-to-file file)
  (log-event-verbosely 'done 'writing)
  (fln "~S bytes written" (stat:size (stat file))))

;;;---------------------------------------------------------------------------
;;; Eval and print

(define (format-error-args args)
  (or (and-let* ((well-formed? (and (list? args)
                                    (list? (cdr args))
                                    (list? (cddr args))
                                    (list? (caddr args))))
                 (fmt (cadr args))
                 (rest (caddr args)))
        (apply fs fmt rest))
      "WARNING: Weird error format!"))

(define (eval-print expr)
  "Evaluate EXPR, a string, and display its value.
EXPR can be the full-path name of a signal or flop,
a partial-path name that is resolved using the current scope,
or a Scheme expression that is passed to Guile using `eval'.
All exceptions are caught in the last case.
If the value of EXPR is unspecified, nothing is written."
  (define ref (lambda (s) (session #:vob (string->symbol s))))
  (let ((result (cond
                 ((ref expr) => variable-ref)
                 ((ref (string-append (session #:scope) expr)) => variable-ref)
                 (else (catch
                        #t
                        (lambda ()
                          (eval-string expr))
                        (lambda (key . args)
                          (format-error-args args)))))))
    (or (unspecified? result)
        (begin
          (write result)
          (newline)))))

;;;---------------------------------------------------------------------------
;;; Deposit

(define-macro (R tag form . body)
  `(catch ,tag (lambda () (eval-in-the-thud-runtime-module ,form))
          (lambda (R-key . R-rest) ,@body)))

(define (deposit details)
  "Parse DETAILS, a string, and deposit value accordingly.
DETAILS should be something like \"/path/to/sig expression\".
Interactively, you do not need to use quotes:

        deposit /path/to/sig expression

If the signal is not absolute (full path), use the current scope as prefix.
If `expression' evaluates to a bit-width wider than the signal, truncate
the value and display a warning.  If `expression' causes an error, deposit
`x' and display a warning.  To purposefully deposit `x', use #f.

Note: Presently, signals named in `expression' must be absolute (full path);
they are not prefixed w/ the scope, as is done for the signal written to.

You probably should not use this from a Scheme program."
  (define (deposit-error s . args)
    (scm-error 'thud-error #f (string-append "deposit: " s) args #f))
  (let* ((cut (string-index details #\space))
         (raw (and cut (string->symbol (substring details 0 cut))))
         (abs? (if (and cut (< 0 cut) raw)
                   (string=? (k #:inm-term) (substring raw 0 1))
                   (deposit-error "Syntax error")))
         (sig (string->symbol (if abs? raw (fs "~A~A" (session #:scope) raw))))
         (expr (with-input-from-string (substring details cut) read))
         (oldval (R 'unbound-variable sig
                    (if abs?
                        (deposit-error "No such signal: ~A" sig)
                        (deposit-error "No such signal ~A in scope ~A"
                                       raw (session #:scope)))))
         (bw (vn-bw sig))
         (val (R #t expr
                 (apply fln (string-append "WARNING: " (cadr R-rest))
                        (caddr R-rest))
                 (fln "DEPOSITED: x  OLDVAL: ~S" (or oldval 'x))
                 #f))
         (vbw (and val (integer-length val))))
    (and bw vbw (< bw vbw)
         (begin
           (fln "WARNING: result width truncated from ~A to ~A" vbw bw)
           (set! val (logand val (1- (ash 1 bw))))))
    (eval-in-the-thud-runtime-module
     `(set! ,sig ,val))))

;;;---------------------------------------------------------------------------
;;; Misc
;;;

(define (cd path)
  "Change directory to PATH and print out current directory."
  (chdir path)
  (fln "~A" (getcwd)))

(define (display-thud-options . msg)
  "Display thud options.  Display optional arg MSG first."
  (maybe-say msg)
  (fln "- thud-opt-debug   ~S" (session #:opt-debug))
  (fln "- thud-opt-verbose ~S" (session #:opt-verbose)))

(define (display-system-info . msg)
  "Display system info.  Display optional arg MSG first."
  (maybe-say msg)
  (fln "- uname\t~S" (array->list (uname)))
  (map (lambda (s)
         (fln "-~S\t~A" s
              (or (false-if-exception (%thud-config-info s)) "???")))
       (%thud-config-info #f))
  (display-thud-options))

(define (thud-exit)
  (throw 'thud-exit))

;;; (thud svc0) ends here
