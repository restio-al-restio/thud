;;; (thud simmath) --- Math using simulation semantics

;; Copyright (C) 1999-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file implements the simulation-specific semantics of certain math
;; operators (including the dreaded `*bofh*').
;;
;; It is R4RS-clean, suitable for compilation using hobbit into a .so file.
;; See the top-level README for discussion.

;;; Code:

(define-module (thud simmath)
  #:export (reduce-init
            reduce
            thud-logxor
            thud-logior
            thud-logand
            thud-plus
            thud-minus
            thud-mult
            thud-div
            thud-modulo
            thud-equal
            thud-shl
            thud-shr
            bx
            Bx
            *bofh*
            c.
            thud-invert))

;; Both `reduce-init' and `reduce' were snarfed from ice-9/common-list.scm.
;;
(define (reduce-init p init l)
  (if (null? l)
      init
      (reduce-init p (p init (car l)) (cdr l))))

(define (reduce p l)
  (cond ((null? l) l)
        ((null? (cdr l)) (car l))
        (else (reduce-init p (car l) (cdr l)))))

;; Some macros for conciseness.
;;
(define-macro (define-chk-ab name func)
  `(define ,name
     (lambda (a b)
       (and a b (,func a b)))))
(define-macro (define-reducer name func)
  `(define ,name
     (lambda args
       (define-chk-ab chk ,func)
       (reduce chk args))))

;; Define THUD variants in terms of system definitions.
;;
(define-reducer thud-logxor logxor)
(define-reducer thud-logior logior)
(define-reducer thud-logand logand)
(define-reducer thud-plus   +)
(define-reducer thud-minus  -)
(define-reducer thud-mult   *)
(define-reducer thud-div    /)

(define-chk-ab thud-modulo modulo)
(define-chk-ab thud-equal  =)
(define-chk-ab thud-shl    ash)
(define-chk-ab thud-shr    (lambda (a b) (ash a (- b))))

(define (bx sig bit)
  (and sig
       (ash (logand sig (ash 1 bit)) (- bit))))

(define (Bx sig bit len)
  (and sig
       (ash (logand sig (ash (1- (ash 1 len)) bit)) (- bit))))

;; For `*bofh*', `bw' is bit-width.  The rest of the args are iteratively
;; interpreted as follows: The car of the list is the VALUE.  If the cadr is
;; negative, use its inverse and the caddr as LEN and BIT, respectively.
;; Otherwise just use the cadr as BIT (with implicit length of 1).  LEN lsb
;; bits from VALUE are accumulated (using `logior' after shifting to BIT
;; position.  Any VALUE that is `#f' causes `*bofh*' to return `#f'.  The
;; return value (unless `#f') is truncated to BW bits.
;;
(define (*bofh* bw . args)
  (let loop ((acc 0)
             (ls  args))
    (if (null? ls)
        (logand (1- (ash 1 bw)) acc)                    ; return truncated
        (let ((value (car ls)))
          (and value                                    ; any #f => return #f
               (let ((hmmm  (cadr ls)))
                 (if (< hmmm 0)
                     ;; handle VALUE LEN BIT
                     (let ((len (- hmmm))
                           (bit (caddr ls)))
                       (loop (logior acc (ash (Bx value 0 len) bit))
                             (cdddr ls)))
                     ;; handle VALUE BIT
                     (let ((bit hmmm))
                       (loop (logior acc (ash (bx value 0) bit))
                             (cddr ls))))))))))

(define (c. . bits)
  (let ((acc 0))
    (for-each (lambda (bit)
                (set! acc (logior (ash acc 1) bit)))
              bits)
    acc))

(define (thud-invert sig)
  (and sig
       (if (zero? sig) 1 0)))

;;; (thud simmath) ends here
