;;; (thud runtime) --- Runtime simulation support

;; Copyright (C) 1998-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This module contains runtime data structures and functional support.
;; It is used for both compiled and interpreted simulation.

;;; Code:

(define-module (thud runtime)
  #:export (eval-in-the-thud-runtime-module
            cc
            set-current-cycle!)
  #:autoload (ice-9 rdelim) (read-line)
  #:use-module (thud simmath)
  #:use-module ((ice-9 gumm) #:select (fob-var
                                       fob-info
                                       current-module
                                       eval-in-current-module-proc))
  #:use-module ((thud state) #:select (session))
  #:use-module ((thud com) #:select (fs
                                     fln
                                     die
                                     maybe-say
                                     n->fw-bstr
                                     thud-load-from-path)))

;; Evaluate EXPR in the runtime environment.
;;
(define eval-in-the-thud-runtime-module (eval-in-current-module-proc))

;;;---------------------------------------------------------------------------
;;; Functional support

(define the-thud-impl-runtime-module (current-module))

(define node-list '())

(define current-cycle #f)

;; Current cycle, a number.
;;
(define (cc) current-cycle)

;; Set current cycle to be @var{val}.
;;
(define (set-current-cycle! val)
  (set! current-cycle val))

(define-macro (fnext! <name> <val>)
  `(or (object-property ',<name> 'user-n)               ; set by `n!'
       (set-object-property! ',<name> 'n ,<val>)))

(define (fnext vn)
  (let ((rv (object-property vn 'n)))
    (set-object-property! vn 'user-n #f)
    rv))

(define-macro (n! <name> <val>)
  `(if (object-property ',<name> 'user-n)
       (fln "THUD-WARNING: ~A ~S ~A ~S ~A ~S ~A"
            "n! used twice -- :name" ,<name>
            ":previous" (object-property ',<name> 'n)
            ":current" ,<val> "-- Preserving previous, ignoring current.")
       (begin
         (set-object-property! ',<name> 'n ,<val>)
         (set-object-property! ',<name> 'user-n #t))))

(define all-flops #f)

(define (one-cycle) #f)                 ; overridden

(define (SIM:run-forever)
  (set! current-cycle 0)
  (let ((cl-args (cdr (command-line))))
    (and=> (member "-M" cl-args)
           (lambda (rest)
             (let ((max (string->number (cadr rest))))
               (and max (number? max) (< 0 max)
                    (add-hook! (session #:aft-cycle)
                               (lambda ()
                                 (and (= current-cycle max)
                                      (throw 'done-sim
                                             (fs "(-M ~A)" max)))))))))
    (and=> (member "--peekfile" cl-args)
           (lambda (rest)
             (let ((peekfile (if (null? (cdr rest))
                                 (error "--peekfile missing filename arg")
                                 (cadr rest))))
               (or (file-exists? peekfile)
                   (error "no such file:" peekfile))
               (add-hook!
                (session #:aft-cycle)
                (let* ((partial '())
                       (add! (lambda (key)
                               (set! partial
                                     (cons (or (hash-ref *peek-thunks* key)
                                               (error "no such node:" key))
                                           partial))))
                       (p (open-input-file peekfile)))
                  (let loop ((line (read-line p)))
                    (if (eof-object? line)
                        (set! partial (reverse! partial))
                        (begin
                          (or (string-null? line)
                              (char=? #\; (string-ref line 0))
                              (add! line))
                          (loop (read-line p)))))
                  (lambda ()
                    (for-each (lambda (thunk) (thunk)) partial)
                    (newline)))))))
    (and=> (member "--peek" cl-args)
           (lambda (rest)
             (add-hook!
              (session #:aft-cycle)
              (if (or (null? (cdr rest))
                      (char=? #\- (string-ref (cadr rest) 0)))
                  display-scheduled-values
                  (let* ((partial '())
                         (add! (lambda (key)
                                 (set! partial
                                       (cons (or (hash-ref *peek-thunks* key)
                                                 (error "no such node:" key))
                                             partial))))
                         (s (cadr rest)))
                    (let loop ((beg 0) (cut (string-index s #\,)))
                      (cond (cut
                             (add! (substring s beg cut))
                             (loop (1+ cut) (string-index s #\, (1+ cut))))
                            (else
                             (add! (substring s beg))
                             (set! partial (reverse! partial)))))
                    (lambda ()
                      (for-each (lambda (thunk) (thunk)) partial)
                      (newline))))))))
  (catch 'done-sim
         (lambda ()
           (let loop ()
             (one-cycle)
             (and (hook? (session #:aft-cycle))
                  (run-hook (session #:aft-cycle)))
             (set! current-cycle (1+ current-cycle))
             (loop)))
         (lambda (key . args)
           (apply fln (apply string-append
                             "SIM:run-forever caught ~S"
                             (make-list (length args) " ~A"))
                  key args))))

(define (define-alias symbol variable)
  ;; deep magic, bad programmer...
  (hashq-set! (fob-info the-thud-impl-runtime-module #:obarray) symbol variable))

(define-macro (defalias sym1 sym2)
  `(define-alias ',sym1 (fob-var the-thud-impl-runtime-module ',sym2)))

(define-macro (define-sig bw name)
  `(begin
     (define ,name #f)
     (hash-set! *peek-thunks* (symbol->string ',name)
                (lambda () (display (fs " ~A" (n->fw-bstr ,bw ,name)))))
     (set! node-list (cons ',name node-list))))

(define d-ref  hashq-ref)

(define (settled-node-list)
  (reverse node-list))

;;; (thud runtime) ends here
