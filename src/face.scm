;;; (thud face) --- Interactive interface

;; Copyright (C) 1998-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file provides interfaces to THUD, the most developed one being
;; `terse-cmd-face'.  All interfaces must: (1) handle a shell escape string
;; of the form "! CMD ARG..." by calling `system' on "CMD ARG..."; (2) finish
;; by calling `thud-exit'.  Other than that, they are free to define their own
;; protocols.
;;
;; User commands are documented in the THUD manual.

;;; Code:

(define-module (thud face)
  #:export (thud-show-face)
  #:use-module ((ice-9 and-let-star) #:select (and-let*))
  #:use-module ((srfi srfi-13) #:select (string-tokenize
                                         string-join
                                         string-take
                                         substring/shared))
  #:use-module ((thud config-info) #:select (%thud-config-info))
  #:use-module ((thud readline-proxy) #:select (set-readline-prompt!
                                                readline))
  #:use-module ((thud runtime) #:select (cc))
  #:use-module ((thud state) #:select (k session))
  #:use-module ((thud svc0) #:select (reset-internals
                                      display-todo-list-reps
                                      display-scheduled-values
                                      display-vob
                                      display-blks-summary
                                      display-instances-summary
                                      display-scopes
                                      add
                                      prepare-to-simulate
                                      next
                                      set-dump-file!
                                      compile-to
                                      format-error-args
                                      eval-print
                                      deposit
                                      display-system-info
                                      thud-exit
                                      cd))
  #:use-module ((thud com) #:select (thud-execute-user-script
                                     fs fln dbg die))
  #:use-module ((thud evlog) #:select (display-event-log)))

;;;---------------------------------------------------------------------------
;;; Abbrev Tree
;;;

;; The abbrev tree is a recursive structure.  Each node is has a `fullname'
;; slot (a string) and a `children' slot (a list).  The latter is initialized
;; as an empty list, and can grow to include either abbrev trees or aliases.
;; See below for info on aliases.

(define ab
  (make-record-type
   'ab
   '(fullname children data)
   (lambda (ab p)
     (with-output-to-port p
       (lambda ()
	 (display (ab-fullname ab))
	 (display (ab-children ab)))))))

(define make-ab
  (lambda (fullname)
    (let ((ret ((record-constructor ab '(fullname)) fullname)))
      (ab-children-set! ret '())
      ret)))

(define ab?     (record-predicate ab))

(define ab-fullname (record-accessor ab 'fullname))
(define ab-children (record-accessor ab 'children))
(define ab-data     (record-accessor ab 'data))

(define ab-children-set! (record-modifier ab 'children))
(define ab-data-set!     (record-modifier ab 'data))

;; An alias is a simple structure holding the alias name and the abbrev tree
;; that the alias stands for.  If an alias is ambiguous, it's `ab' slot holds
;; the list of possible abbrev tree fullnames rather than a single tree.  The
;; creation of aliases, then, must occur once all children are known and not
;; before.

(define aba
  (make-record-type
   'aba
   '(alias ab)
   (lambda (aba p)
     (with-output-to-port p
       (lambda ()
	 (display (aba-alias aba)) (display "%")
	 (display (let ((hmm (aba-ab aba)))
		    (if (ab? hmm)
			(ab-fullname hmm)
			hmm))))))))

(define make-aba (record-constructor aba '(alias ab)))
(define aba?     (record-predicate aba))

(define aba-alias (record-accessor aba 'alias))
(define aba-ab   (record-accessor aba 'ab))

(define (scrub string)
  ;; Return new string, the result of canonicalizing STRING.
  ;; This means removing surrounding whitespace and changing internal
  ;; contiguous spaces, tabs and newlines to a single space.
  (string-join (string-tokenize string) " "))

(define (split-first-word scrubbed-string)
  ;; Return a pair (FIRST-WORD . REST-OF-STRING) from SCRUBBED-STRING.
  (cond ((string-index s #\space)
         => (lambda (mid)
              (cons (string-take s mid)
                    (substring/shared s (1+ mid)))))
        (else
         (cons s ""))))


(define (list->sep-string list sep)
  ;; Return LIST as a string w/ SEP between elements.
  (with-output-to-string
    (lambda ()
      (let loop ((ls list))
        (and (pair? ls)
             (begin
               (display (car ls))
               (display (if (pair? (cdr ls)) sep ""))
               (loop (cdr ls))))))))

(define (gdb-style-ambiguous name tree choices)
  ;; Display an "ambiguous command" message a la gdb.
  (display "Ambiguous ")
  (let ((fn (ab-fullname tree)))
    (or (string-null? fn)
        (begin (display fn) (display " "))))
  (display "command \"")
  (display name)
  (display "\": ")
  (display (list->sep-string choices ", "))
  (display ".") (newline))

(define (gdb-style-undefined name tree)
  ;; Display an "undefined command" message a la gdb.
  (display "Undefined")
  (let* ((fulln (ab-fullname tree))
         (fn (if (string-null? fulln)
                 ""
                 (string-append " " fulln))))
    (display fn)
    (display " command: \"")
    (display name)
    (display "\".  Try \"help")         ; assumes "help" is implemented
    (display fn)
    (display "\".")
    (newline)))

(define (whatever . args) "Return #t." #t)

;; Climbing on the tree.  Does a branch hold or will it break?

(define (abtree-climb name tree sturdy ambiguous shaky . so-far)
  ;; See if NAME is visible as a child in TREE, as either an abbrev tree
  ;; or an alias.  If found, STURDY is called with the fullname of the child.
  ;; If not found, SHAKY is called with NAME and TREE as args.  If there is
  ;; ambiguity, AMBIGUOUS is called with args NAME, TREE and a list of
  ;; possible fullnames.
  ;;
  ;; At this time, the optional arg SO-FAR is not used, but it will probably
  ;; be tacked on as the optional arg for STURDY, SHAKY and AMBIGUOUS at some
  ;; point (as a place for caller-defined data).
  ;;
  ;; children that are neither abbrev trees nor aliases
  ;; throw a `bad-child' error.
  '(for-each display (list "looking for " name
                           " in tree <" (ab-fullname tree) ">"))
  '(newline)
  (call-with-current-continuation
   (lambda (return)
     (or (ab? tree)
         (scm-error 'bad-tree 'abtree-climb
                    "not really a tree: %s" tree #f))
     (for-each
      (lambda (child)
        (cond ((ab? child)
               (let ((fn (ab-fullname child)))
                 (and (string=? name fn)
                      (return (sturdy child)))))
              ((aba? child)
               (let ((fn (aba-ab child)))
                 (and (string=? name (aba-alias child))
                      (if (list? fn)
                          (return (ambiguous name tree fn))
                          (return (sturdy fn))))))
              (else
               (scm-error 'bad-child 'abtree-climb
                          "neither ab nor aba: %s" child #f))))
      (ab-children tree))
     (return (shaky name tree)))))

;; Adding to the tree

(define (abtree-add tree fullname-path data)
  ;; Add to TREE by splitting up FULLNAME-PATH into constituent words
  ;; and creating children trees.  FULLNAME-PATH is scrubbed before
  ;; splitting.  If FULLNAME-PATH already has been added, an error is
  ;; signalled.  [But what about `DATA' ?!?]
  (let* ((split (split-first-word (scrub fullname-path)))
         (word (car split)))
    ;;(display "adding ") (display split) (newline) (force-output)
    (if (string-null? word)
        (ab-data-set! tree data)
        (let ((lookup (abtree-climb word tree
                                    identity
                                    (lambda (name tree choices)
                                      (error name tree choices))
                                    (lambda (name tree) #f))))
          (if lookup
              (and (string-null? (cdr split))
                   (error "already have it!"))
              (begin
                (set! lookup (make-ab word))
                (ab-children-set! tree (cons lookup (ab-children tree)))))
          (abtree-add lookup (cdr split) data)))))

(define (abtree-import in-data)
  ;; Translate IN-DATA, a nested list, into an abbrev tree and return it.
  ;; For each list in IN-DATA, the car becomes a fullname and the CDR the
  ;; children.  Elements of the cdr can be lists, in which case
  ;; `abtree-import' is called recursively.  This procedure is experimental
  ;; and may go away.
  (if (list? in-data)
      (let ((ret (make-ab (car in-data))))
        (ab-children-set! ret (map abtree-import (cdr in-data)))
        ret)
      (make-ab in-data)))

(define (triangle name)
  ;; Return a list of all successively shorter substrings of NAME.
  ;; NAME is also included, as the car of the list.
  (let ((len (string-length name)))
    (if (= 1 len)
        (list name)
        (cons name (triangle (string-take name (1- len)))))))

(define (abtree-expand tree)
  ;; Recursively add alias children to TREE.  Each alias's `ab' slot is
  ;; either an abbrev tree when unambiguous, or a list of fullnames of
  ;; possible abbrev trees, when ambiguous.  Only TREE's children are
  ;; consulted and referenced.
  (or (ab? tree) (error "bad tree error"))
  (for-each
   (lambda (elem)
     (ab-children-set! tree (cons (make-aba (car elem) (cdr elem))
                                  (ab-children tree))))
   (let ((alias-alist '()))
     (for-each
      (lambda (child)
        (and (ab? child)
             (begin
               (abtree-expand child)
               (for-each
                (lambda (tri)
                  (let ((seen (assoc tri alias-alist))) ; assq doesn't work
                    (if seen
                        (set-cdr! seen
                                  (cons (ab-fullname child)
                                        (let ((rest (cdr seen)))
                                          (if (ab? rest)
                                              (list (ab-fullname rest))
                                              rest))))
                        (set! alias-alist
                              (acons tri child alias-alist)))))
                (cdr (triangle (ab-fullname child)))))))
      (ab-children tree))
     alias-alist))
  tree)

;;;---------------------------------------------------------------------------
;;; User variables

(define-macro (defuvar name location docstring . val-filter)
  (let* ((context (list-ref location 0))
         (cx-name (list-ref location 1))
         (setter (symbol-append 'set-  name '!))
         (getter (symbol-append 'get-  name))
         (wrline (symbol-append 'echo- name))
         (sbody  `(lambda (val)
                    ,(fs "Set `~A' to VAL.\n~A" name docstring)
                    (set! (,context ,cx-name)
                          (,(if (null? val-filter)
                                'identity
                                (car val-filter))
                           val)))))
    `(define (,(symbol-append 'uvar: name) selection)
       ;; setter
       (define ,setter ,sbody)
       ;; getter
       (define ,getter (lambda ()
                         ,(fs "Return value of `~A'.\n~A" name docstring)
                         (,context ,cx-name)))
       ;; echo
       (define ,wrline (lambda ()
                         ,(fs "~A `~A'.\n~A\n~A `~A' ~A."
                              "Display value of" name
                              docstring
                              "Use" getter "to get the value without display.")
                         (fln "~A" (,context ,cx-name))))
       ;; do it!
       (case selection
         ((#:setter) ,setter)
         ((#:getter) ,getter)
         ((#:wrline) ,wrline)))))

(defuvar batch-mode (session #:opt-batch)
  "Non-#f value tells THUD to exit after reading the initialization file
and processing the command line.  The command line option `-b' sets it.")

(defuvar verbose (session #:opt-verbose)
  "Non-#f value tells THUD to say gratuitous things.")

(defuvar debug (session #:opt-debug)
  "Integer value, with 0 meaning no debug information is displayed
during operation.  Invoke thud with the option \"-i dbglvls\" to see
values that can be bit-wise OR'ed together.  This variable is primarily
for use by THUD developers.")

(defuvar init-file (session #:opt-init-file)
  "File name that is sourced using `thud-execute-user-script' during
initialization (that is, if the file exists).")

(defuvar face (session #:opt-face)
  "Symbol specifying an interactive face to use.")

(defuvar max-cycle (session #:cmax)
  "Cycle number (or expression?) where sim stops, #f means never.")

(defuvar scope (session #:scope)
  "Name of an instance.
WARNING: Scope semantics are still being evaluated."
  (lambda (v)
    ;;     "Set scope to NEW-SCOPE.  The name is canonicalized to include
    ;; a trailing instance-name terminator (aka \"path separator\")."
    (let* ((ok-names (map (lambda (pair)
                            (symbol->string (car pair)))
                          (session list #:instances)))
           (rv (cond ((or (string-null? v)
                          (string=? v (k #:top-level-name)))
                      (k #:top-level-name))
                     ((member v ok-names) => car)
                     ((member (in-vicinity v "") ok-names) => car)
                     (else
                      (scm-error 'thud-error #f
                                 "bad scope: ~A (try ~S)"
                                 (list v "info scope") #f)))))
      (fln "~A" rv)
      rv)))

(defuvar dump-file (session #:vcd-file)
  "File name where VCD output is written.")

;;;---------------------------------------------------------------------------
;;; Help
;;;

(define (one-line-docstring proc)
  (and-let* ((docstr (procedure-documentation proc))
             (len (string-length docstr))
             (cut (min (or (string-index docstr #\newline) len)
                       (or (string-index docstr #\.) len))))
    (and (not (= cut len))
         (substring docstr 0 cut))))

(define (full-docstring proc)
  (let* ((name    (procedure-name proc))
         (formals (cadr (procedure-source proc)))
         (flist   (cond ((null? formals)
                         '(""))
                        ((not (pair? formals))
                         `(" [" ,(symbol->string formals) " ...]"))
                        ((list? (last-pair formals))
                         `(" " ,(list->sep-string formals " ")))
                        (else
                         `(" "
                           ,(list->sep-string formals " ")
                           " ["
                           ,(symbol->string (cdr (last-pair formals)))
                           " ...]"))))
         (fstr    (string-upcase (apply string-append flist)))
         (terse-c (object-property proc 'terse-cmd)))
    (string-append
     (or (procedure-documentation proc)
         "(no documentation, bummer!)")
     "\n\n"
     (if terse-c
         (fs "From the `terse-cmd-face' prompt, use:\n~A~A" terse-c fstr)
         "(cannot be called from `terse-cmd-face', bummer!)")
     "\n\n"
     (if name
         (fs "From a Scheme program, use:\n(~A~A)" name fstr)
         "(cannot be called from Scheme, bummer!)")
     "\n")))

(define (gdb-style-help-generic-command path real-kids)
  (let ((cmd (list->sep-string (cdr path) " ")))
    (fln "~S is a generic command.\n" cmd)
    (map (lambda (child)
           (fln "~A -- ~A"
                (ab-fullname child)
                (one-line-docstring (cadr (ab-data child)))))
         real-kids)
    (fln "\nType ~S followed by a subcommand for details."
         (string-append "help " cmd))
    (fln "Unambiguous abbreviations are ok.")))

;;;---------------------------------------------------------------------------
;;; Error handling

(define (face-handler key . args)
  (case key
    ((system-error) (fln "~A" (format-error-args args)))
    ((thud-error)   (fln "~A" (format-error-args args)))
    ((thud-exit)    (thud-exit))
    ((quit)         (thud-exit))
    (else           (fln "~A\n\t ~A\n\t ~S ~S"
                         "WARNING: Exception not handled by `face-handler'!"
                         "Please report this bug."
                         key args))))

;;;---------------------------------------------------------------------------
;;; Prompt

(define thud-prompt "\n(thud) ")        ; roundness is ok

(define (format-prompt)
  (list->string
   (reverse
    (let* ((acc '())
           (push! (lambda (x) (set! acc (cons x acc))))
           (bapp! (lambda (x) (set! acc (append (reverse x) acc)))))
      (let loop ((ls (string->list thud-prompt)))
        (and (pair? ls)
             (begin
               (if (char=? #\% (car ls))
                   (if (or (eq? '() (cdr ls)) (char=? #\% (cadr ls)))
                       (push! #\%)
                       (begin
                         (case (string->symbol (make-string 1 (cadr ls))) ;yuk
                           ((c) (bapp! (string->list (number->string (cc)))))
                           ((s) (bapp! (string->list scope)))
                           (else (push! #\%)
                                 (push! (cadr ls))))
                         (set! ls (cdr ls))))
                   (push! (car ls)))
               (loop (cdr ls)))))
      acc))))

(define (set-thud-prompt! new-prompt)
  "Set prompt to NEW-PROMPT.  When displayed using `format-prompt' (q.v.),
\"%c\" is replaced by the current cycle number and \"%s\" is replaced by
the current scope."
  (set! thud-prompt (eval-string new-prompt)))

;;;---------------------------------------------------------------------------
;;; Full power top-level repl face

(define (full-face)
  (set! scm-repl-prompt format-prompt)
  (use-modules (thud com) (thud scan) (thud sched) (thud sim)
               ((thud runtime) :select (cc))
               (thud svc0))
  ;; TODO: Using `scm-style-repl' means shell-escape requirement is not met.
  ;; TODO: How to handle `die'.
  (scm-style-repl)
  (thud-exit))

;;;---------------------------------------------------------------------------
;;; Terse syntax repl face

(define terse-cmd-table
  `(;; See `parse' in `terse-cmd-face' for interpretation:
    ;; COMMAND-STRING   PCTL    WORK-PROC
    ("x"                s       ,thud-execute-user-script)
    ("add"              s       ,add)
    ("link"             r       ,prepare-to-simulate)
    ("next"             i*1     ,next)
    ("reset"            r       ,reset-internals)
    ("quit"             r       ,thud-exit)
    ("print"            s       ,eval-print)
    ("deposit"          s       ,deposit)
    ("cd"               s       ,cd)

    ("set"              g       #f)
    ("set verbose"      b       ,(uvar:verbose #:setter))
    ("set debug"        i       ,(uvar:debug #:setter))
    ("set max-cycle"    i       ,(uvar:max-cycle #:setter))
    ("set dump-file"    s       ,set-dump-file!)
    ("set scope"        s       ,(uvar:scope #:setter))
    ("set prompt"       s       ,set-thud-prompt!)

    ("show"             g       #f)
    ("show working-dir" r       ,(lambda () "Display working directory."
                                         (fln "~A" (getcwd))))
    ("show system"      r       ,display-system-info)
    ("show version"     r       ,(lambda () "Display version."
                                         (fln "~A" (%thud-config-info
                                                    'VERSION))))
    ("show batch"       r       ,(uvar:batch-mode #:wrline))
    ("show verbose"     r       ,(uvar:verbose #:wrline))
    ("show debug"       r       ,(uvar:debug #:wrline))
    ("show warranty"    r       ,(lambda () "Display warranty info."
                                         (fln "NO WARRANTY!")))
    ("show scope"       r       ,(uvar:scope #:wrline))
    ("show max-cycle"   r       ,(uvar:max-cycle #:wrline))
    ("show dump-file"   r       ,(uvar:dump-file #:wrline))
    ("show cc"          r       ,(lambda () "Display cc." (fln "~A" (cc))))
    ("show event-log"   r       ,display-event-log)
    ("show prompt"      r       ,(lambda ()
                                   "Display value of `thud-prompt'."
                                   (write thud-prompt) (newline)))

    ("info"             g       #f)
    ("info blks"        r       ,display-blks-summary)
    ("info vob"         r       ,display-vob)
    ("info todolist"    r       ,display-todo-list-reps)
    ("info schedvals"   r       ,display-scheduled-values)
    ("info instances"   r       ,display-instances-summary)
    ("info scopes"      r       ,display-scopes)

    ("fs"               g       #f)
    ("fs compile"       s       ,compile-to)
    ))

(define (terse-cmd-face)

  (define (cspfold ls)                  ; comma, space and folded
    (let* ((w (cond ((getenv "COLUMNS") => string->number)
                    ((getenv "WIDTH") => string->number)
                    (else 80)))
           (s (list->sep-string ls ", "))
           (len (string-length s)))
      (let loop ((idx 0) (right w))
        (and (< right len)
             (let ((comma (string-rindex s #\, idx right)))
               (string-set! s (1+ comma) #\newline)
               (loop (1+ comma) (+ comma w)))))
      s))

  (let ((root (make-ab ""))
        (help (make-ab "help")))

    (define (prompt-and-readline)
      (set-readline-prompt! (format-prompt))
      (readline))

    (define (one-on string)
      (substring string 1 (string-length string)))

    (define (arg-key->name key)
      (case key
        ((b) "boolean")
        ((i) "integer")
        ((s) "string")
        ((x) "extended")
        ((i*1) "integer (default 1)")
        (else "more")))

    (define (parse-boolean string)
      (cond ((member string '("#t" "true" "1" "on"))
             #t)
            ((member string '("#f" "false" "0" "off"))
             #f)
            (else
             (throw 'parse-error))))

    (define (parse-integer string)
      (or (string->number string)
          (throw 'parse-error)))

    (define (parse-extended string)
      (fln "[for now, parse-extended just returns string]")
      string)

    (define (parse input root acc)
      (and-let* ((s    (scrub input))
                 (tree (if (equal? '() acc) root (car acc)))
                 (data (ab-data tree))
                 (pctl (car data))      ; parse control
                 (proc (or (cadr data) identity)))
        (dbg DBGUSR
             (fln "~A ~A <~A>" (reverse (map ab-fullname acc)) pctl s))

        (if (string-null? s)
            (let* ((path (reverse (map ab-fullname acc)))
                   (help? (string=? "help" (car path))))
              (case pctl
                ;; generic command: show subcommands
                ((g) (let ((real-kids (let loop ((ls (ab-children tree))
                                                 (acc '()))
                                        (if (null? ls)
                                            (reverse! acc)
                                            (loop (cdr ls)
                                                  (if (aba? (car ls))
                                                      acc
                                                      (cons (car ls)
                                                            acc)))))))
                       (if help?
                           (if (= 1 (length path))
                               (let ((type "Type ~S to ~A."))
                                 (fln type ",EXPR" "eval a Scheme EXPR")
                                 (fln type "!CMD" "run a shell command")
                                 (fln type "CMD" "run THUD commands")
                                 (fln "Here are all the commands:\n\n~A.\n"
                                      (cspfold (map car terse-cmd-table)))
                                 (fln "Type ~S ~A.\n~A" "help"
                                      "followed by a command for details"
                                      "Unambiguous abbreviations are ok."))
                               (gdb-style-help-generic-command path real-kids))
                           (gdb-style-ambiguous (ab-fullname tree)
                                                tree
                                                (map ab-fullname real-kids)))))
                ;; input recognized completely, act on it
                ((r) (if help?
                         (display (full-docstring proc))
                         (proc)))
                ;; incomplete
                (else (if help?
                          (display (full-docstring proc))
                          (if (eq? pctl 'i*1) ; sigh
                              (proc 1)
                              (fln "Incomplete input, need ~A arg.  Try ~S."
                                   (arg-key->name pctl)
                                   (string-append
                                    "help " (list->sep-string
                                             (reverse (map ab-fullname acc))
                                             " "))))))))
            (if (memq pctl '(b i s x i*1))
                (let ((args (catch
                             'parse-error
                             (lambda ()
                               (case pctl
                                 ((b) (parse-boolean s))
                                 ((i) (parse-integer s))
                                 ((i*1) (parse-integer s))
                                 ((s) s)
                                 ((x) (parse-extended s))))
                             (lambda args
                               (scm-error
                                'thud-error #f "Improper ~A: ~A"
                                (list (arg-key->name pctl) s) #f)))))
                  (proc args))
                (let ((split (split-first-word s)))
                  (abtree-climb (car split) root
                                (lambda (child)
                                  ;; sturdy
                                  (parse (cdr split)
                                         child
                                         (cons child acc)))
                                ;; ambiguous
                                gdb-style-ambiguous
                                ;; shaky
                                gdb-style-undefined))))))

    ;; Initialize trees.
    ;;
    (for-each (lambda (ent)
                (abtree-add root (car ent) (cdr ent))
                (let ((proc (caddr ent)))
                  (and (procedure? proc)
                       (set-object-property! proc 'terse-cmd (car ent)))))
              terse-cmd-table)
    (set! root (abtree-expand root))
    (ab-data-set!     root '(g #f))
    (ab-children-set! help (ab-children root))
    (ab-data-set!     help '(g #f))
    (ab-children-set! root (append (list
                                    help
                                    (make-aba 'h   help)
                                    (make-aba 'he  help)
                                    (make-aba 'hel help))
                                   (ab-children root)))

    ;; Go into repl.
    ;;
    (let loop ((input (prompt-and-readline)))
      (or (eof-object? input)
          (begin
            (set! input (scrub input))
            (cond ((string-null? input))
                  ((char=? #\, (string-ref input 0))
                   (eval-print (one-on input)))
                  ((char=? #\! (string-ref input 0))
                   (let ((shell-command (one-on input)))
                     (and (string-null? shell-command)
                          (set! shell-command (getenv "SHELL")))
                     (system shell-command)))
                  (else (catch #t
                               (lambda () (parse input root '()))
                               face-handler)))
            (loop (prompt-and-readline)))))))

;;;---------------------------------------------------------------------------
;;; Special face for Emacs
;;; WARNING: this is still experimental!

(define (emacs-face)
  (and (session #:opt-verbose) (fln "user: Connected to THUD"))
  (set! scm-repl-prompt "<experimental-emacs> ")
  (scm-style-repl)
  (thud-exit))

;;;---------------------------------------------------------------------------
;;; Showing face

;; Here is where we register the names of all the faces that can be shown.
;;
(define all-faces
  `((full ,full-face "thin layer over implementation platform")
    (terse-cmd ,terse-cmd-face "terse syntax, including abbreviations")
    (emacs ,emacs-face "experimental, for emacs")))

;; Showing face is what it's all about.
;; If NAME is #t, return a list of (NAME . DESCRIPTION) pairs.
;; If NAME is #f, use the `terse-cmd' face.
;;
(define (thud-show-face name)
  (cond ((eq? #t name)
         (map (lambda (entry)
                (cons (list-ref entry 0) (list-ref entry 2)))
              all-faces))
        ((assq-ref all-faces (or name 'terse-cmd))
         => (lambda (thunk+description) ((car thunk+description))))
        (else
         (die "invalid face"))))

;;; (thud face) ends here
