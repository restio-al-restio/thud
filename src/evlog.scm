;;; (thud evlog) --- Event log procedures

;; Copyright (C) 1999-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(define-module (thud evlog)
  #:export (log-event
            log-event-verbosely
            display-event-log)
  #:use-module ((thud state) #:select (session))
  #:use-module ((thud com) #:select (human-readable-time-stamp
                                     fln maybe-say t2)))

(define (log-event tag data)
  "Log event TAG and DATA in the event log.
Normally, both TAG and DATA are recorded verbatim in the log.

As a special case, if TAG is the symbol `done', DATA is taken as a symbol to
search back through the event log for.  Then, instead of recording TAG and
DATA normally, the time since the matching event is computed and this value is
recorded as TIME and UNITS.  Thus, a useful idiom is:

        (log-event 'processing 'something)
        ... (do-processing) ...
        (log-event 'done 'processing)

Note the second call to `log-event'."
  (define (n t) (+ (* 1000000 (car t)) (cdr t)))
  (let ((cur (gettimeofday))
        (diff #f))
    (cond ((eq? tag 'done)
           (set! diff (/ (- (n cur)
                            (n (let loop ((ls (session #:log)))
                                 (if (eq? data (cadar ls))
                                     (caar ls)
                                     (loop (cdr ls))))))
                         1000000))
           (cond ((> diff 60)
                  (set! tag  diff)
                  (set! data (list (t2 (/ tag 60)) 'min)))
                 ((< diff 1)
                  (set! tag  (* diff 1000))
                  (set! data 'msec))
                 (else
                  (set! tag  diff)
                  (set! data 'sec)))))
    (set! (session #:log) (cons (list cur tag data) (session #:log)))
    diff))

(define (display-log-entry e)
  (fln "~A -- ~A ~A"
       (human-readable-time-stamp (localtime (caar e)))
       (cadr e)
       (or (caddr e) "")))

(define (log-event-verbosely tag data)
  "Log event TAG and DATA in the event log, verbosely.
See `log-event' for more information."
  (let ((ret (log-event tag data)))
    (display-log-entry (car (session #:log)))
    ret))

(define (display-event-log . msg)
  "Display event log.  Display optional arg MSG first.
Each line in the log has the format `DATE DAY-OF-WEEK TIME -- EVENT'."
  (maybe-say msg)
  (for-each display-log-entry (reverse (session #:log))))

;;; (thud evlog) ends here
