;;; (thud permute) --- Run specified instance w/ permuted input ports

;; Copyright (C) 1998-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commetary:

;; (1) See note in `log-done-permuting-event!'
;; (2) We do not use `dbg' (macro); instead consult env var `THUD_DEBUG'.

;;; Code:

(define-module (thud permute)
  #:export (permute)
  #:use-module ((thud com) #:select (t2))
  #:use-module ((thud svc0) #:select (prepare-to-simulate
                                      display-scheduled-values
                                      next))
  #:use-module ((thud runtime) #:select (eval-in-the-thud-runtime-module))
  #:use-module ((thud evlog) #:select (log-event-verbosely
                                       log-event))
  #:use-module ((thud types) #:select (d-ref
                                       d->list
                                       instance-blk instance-values
                                       enof-bw vn-enof
                                       blk-ports
                                       pdir))
  #:use-module ((thud state) #:select (session)))

(define (log-done-permuting-event! bw)
  ;; Note: hobbit-1.3.4 assumes bounded integer arithmetic,
  ;;       and so mis-handles the floats.
  (let* ((time (log-event 'done 'permuting))
         (sec/cycle (/ time (ash 1 bw))))
    (log-event (t2 (* 1000 sec/cycle)) 'msec/cycle)
    (log-event (t2 (/ 1 sec/cycle))    'cycle/sec)))

(define (permute . inm-list)
  "Simulate INSTANCE-NAME-LIST with permuted input ports.
The instances should be in the `(session #:instances)'."

  (let ((names '())                     ; signals
        (xbegs '())                     ; bit-extract beginning positions
        (xends '())                     ; bit-extract ending positions
        (bw     0)                      ; cumulative bit width
        (count  #f)                     ; how many cycles to simulate (2 ^ bw)
        (jammer #f))                    ; thunk to set values each cycle

    ;; Determine inputs and bw.
    (prepare-to-simulate)
    (for-each
     (lambda (inm)
       (let* ((instance (session #:instances inm))
              (blk      (instance-blk instance))
              (ports    (blk-ports blk)))
         (for-each
          (lambda (pname-vn)
            (let* ((pname (car pname-vn))
                   (vn    (cdr pname-vn))
                   (p     (d-ref ports pname)))
              (and p (eq? 'in (pdir p))
                   (begin
                     (set! names (cons (symbol-append inm pname) names))
                     (set! xbegs (cons bw xbegs))
                     (set! xends (cons (+ bw (enof-bw (vn-enof vn))) xends))
                     (set! bw (car xends))))))
          (d->list (instance-values instance)))))
     inm-list)

    ;; Add hooks.
    (set! jammer
          (let ((jam (eval-in-the-thud-runtime-module
                      `(lambda (count)
                         ,@(map (lambda (ip b e)
                                  `(set! ,ip (bit-extract count ,b ,e)))
                                names xbegs xends))))
                (dot (if (getenv "THUD_DEBUG")
                         (lambda () #f)
                         (lambda ()
                           (and (zero? (modulo count 100))
                                (display #\.)
                                (force-output))))))
            (lambda ()
              (jam count)
              (dot)
              (set! count (1+ count)))))
    (add-hook! (session #:bef-cycle) jammer)
    (and (getenv "THUD_DEBUG")
         (add-hook! (session #:aft-cycle) display-scheduled-values))

    ;; Do it!
    (log-event-verbosely 'permuting
                         (append inm-list (list (ash 1 bw) 'cycles)))
    (set! count 0)
    (next (ash 1 bw))
    (newline)
    (log-done-permuting-event! bw)

    ;; Remove hooks.
    (and (getenv "THUD_DEBUG")
         (remove-hook! (session #:aft-cycle) display-scheduled-values))
    (remove-hook! (session #:bef-cycle) jammer)))

;;; (thud permute) ends here
