;;; (thud readline-proxy) --- Support readline calls, but do less

;; Copyright (C) 1998-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file provides `readline', `set-readline-prompt!' and `add-history'.
;; The first two are self-explanatory, the last merely a stub.

;;; Code:

(define-module (thud readline-proxy)
  #:export (set-readline-prompt!
            readline
            add-history)
  #:autoload (ice-9 rdelim) (read-delimited))

(define fake-readline-prompt #f)

(define (set-readline-prompt! s)
  (set! fake-readline-prompt s))

(define (readline)
  (display fake-readline-prompt)
  (force-output)
  (read-delimited "\n"))

(define (add-history ignore)
  #f)

;;; (thud readline-proxy) ends here
