;;; (thud thgen) --- THUD HDL generation.

;; Copyright (C) 1999-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; TODO: Think about CSDL (if published) or maybe SLED (if not) bindings.

;;; Commentary:

;; Algorithm is pretty simple: set configuration then generate the list of
;; instances (each a list of forms) for DEVICE.  CONFIG has defaults that
;; are set at load-time, not hard-coded.  For some variables, there are
;; consistency checks run after their setting; these may fail and throw an
;; exception (see below).
;;
;; All exceptions thrown use key `thud-thgen-error' as the first argument
;; to procedure `scm-error'.

;;; Code:

(define-module (thud thgen)
  #:export (thgen
            thgen/command-line-rest
            define-generator
            format-symbol)
  #:autoload (ice-9 getopt-long) (getopt-long)
  #:use-module ((thud state) #:select (make-context))
  #:use-module ((thud com) #:select (fs fln)))

;;;---------------------------------------------------------------------------
;;; support

(define (thgen-error s . args)
  (scm-error 'thud-thgen-error #f s args #f))

(define (no-such-var-error var)
  (thgen-error "No such config var: ~S" var))

(define (format-symbol s . args)
  (string->symbol (apply fs s args)))

;;;---------------------------------------------------------------------------
;;; Type checking

(define (apply-to-args args proc) (apply proc args)) ; FIXME: Centralize.

(define *type-checkers*
  (map (lambda (x)
         (apply-to-args
          x (lambda (type check-actual maybe-convert)
              (cons type
                    (lambda (var val)
                      (let ((actual (if (string? val)
                                        (maybe-convert val)
                                        val)))
                        (or (check-actual actual)
                            (thgen-error "Value for `~A' not a ~A: ~S"
                                         var (keyword->symbol type) actual))
                        actual))))))
       `((#:string ,string? ,identity)
         (#:integer ,integer? ,string->number)
         (#:symbol ,symbol? ,string->symbol)
         (#:sexp ,(lambda x x) ,(lambda (s) (call-with-input-string s read))))))

;;;---------------------------------------------------------------------------
;;; Configuration

(define SCRATCH (make-context '((#:vars #t 11)
                                (#:templates #t 11)
                                (#:var-alists #t 11)
                                (#:generators #t 11))))

(define (C:name  entry) (car entry))
(define (C:value entry) (cadr entry))
(define (C:opts  entry) (cddr entry))
(define (C:type  entry) (car (C:opts entry)))

(define (C:val! entry new)
  (set-car! (cdr entry) new))

(define (default-config)
  (copy-tree '((seed        0      #:integer)
               (seq-beg     0      #:integer)
               (seq-end    15      #:integer)
               (width       1      #:integer)
               (block-name #:UNSET #:symbol))))

(define (var-entry configs var)
  (or-map (lambda (config)
            (assq var config))
          configs))

(define (config-lookup configs var)
  (cond ((var-entry configs var) => C:value)
        (else #f)))

(define (set-config-var! configs var val)
  (let* ((entry (or (var-entry configs var)
                    (no-such-var-error var)))
         (check (assq-ref *type-checkers* (C:type entry))))
    (C:val! entry (check var val))))

(define (merge-config base edits)
  (and (eq? #t base) (set! base (list (default-config))))
  (for-each (lambda (edit)
              (set-config-var! base (car edit) (cdr edit)))
            edits)
  base)

;;;---------------------------------------------------------------------------
;;; Generation

(define (make-generator template up-va)
  ;; rv
  (lambda (configs)
    (let* ((new (copy-tree template))
           (beg (config-lookup configs 'seq-beg))
           (end (config-lookup configs 'seq-end))
           (get (lambda (var) (config-lookup configs var))))
      (map (lambda (seed)
             (set-config-var! configs 'seed seed)
             (for-each (lambda (va)
                         (set! configs (merge-config configs (va get))))
                       up-va)
             (default-expander seed new configs))
           (map (lambda (n)
                  (+ n beg))
                (iota (- (1+ end) beg)))))))

(define (define-generator name . args)

  (define (gen-error s . args)
    (apply thgen-error (string-append "~A: " s) name args))

  (define (kw-ref kw . default)
    (or (kw-arg-ref args kw)
        (and (not (null? default)) (car default))
        (gen-error "Missing required spec: ~A" kw)))

  ;; do it!
  (set! (SCRATCH #:var-alists name) (kw-ref #:var-alist-proc))
  (let loop ((up-v #f)
             (tem (kw-ref #:template))
             (up-va '()))
    (cond ((symbol? tem)
           (let ((up (or (SCRATCH #:templates tem)
                         (gen-error "No such base device: ~A" tem))))
             (loop (or up-v (SCRATCH #:vars tem))
                   up
                   (cons (SCRATCH #:var-alists tem) up-va))))
          (else
           (set! (SCRATCH #:vars name)
                 (cons (let ((spec (kw-ref #:vars '())))
                         (and-map (lambda (entry)
                                    (let ((type (C:type entry)))
                                      (or (assq type *type-checkers*)
                                          (gen-error
                                           "Var `~A' has bad type: ~S"
                                           (C:name entry) type))))
                                  spec)
                         spec)
                       (or up-v
                           (list (default-config)))))
           (set! (SCRATCH #:templates name)
                 tem)
           (set! (SCRATCH #:generators name)
                 (make-generator (SCRATCH #:templates name)
                                 (reverse! (cons (SCRATCH #:var-alists name)
                                                 up-va))))))))

(define (default-expander seed new config-in)
  (define (config-ref var)
    (or (config-lookup config-in var)
        (no-such-var-error var)))

  (define (ex form)
    (if (list? form)
        (let ((res '()))
          (for-each
           (lambda (elem)
             (or (and (list? elem)
                      (let ((op (car elem)))
                        (cond ((eq? 'unquote op)
                               (set! res (cons (config-ref (cadr elem)) res))
                               ;; adding `ex' ^ here being considered.
                               #t)
                              ((eq? 'unquote-splicing op)
                               (for-each (lambda (fully-deref)
                                           (set! res (cons fully-deref res)))
                                         (map ex (config-ref (cadr elem))))
                               #t)
                              (else
                               (set! res (cons (ex elem) res))))))
                 (set! res (cons elem res))))
           form)
          (reverse! res))
        form))

  (ex new))

;;;---------------------------------------------------------------------------
;;; Built-in generators

(define-generator 'mux

  #:template '((blk ,block-name
                    (out ,width x)
                    (in  ,width a0)
                    (in  ,width a1)
                    (in  sel))
               (a. x (| (& (! sel) a0) (& sel a1))))

  #:var-alist-proc (lambda (get)
                     (let ((seed (get 'seed)))
                       `((block-name . ,(format-symbol "mux~A" seed))
                         (width . ,seed)))))

(define-generator 'flop

  #:template '((blk ,block-name
                    (out ,width q)
                    (in  ,width d)
                    (in         en))
               (reg ,width state)
               (a. q state)
               (n. state (| (&    en  d)
                            (& (! en) state))))

  #:var-alist-proc (lambda (get)
                     (let ((seed (get 'seed)))
                       `((block-name . ,(format-symbol "flop~A" seed))
                         (width . ,seed)))))

;;;---------------------------------------------------------------------------
;;; Driver

(define (thgen device specifically)
  (cond ((SCRATCH #:generators device)
         => (lambda (gen!!!)
              (gen!!! (merge-config (SCRATCH #:vars device) specifically))))
        (else #f)))

(define (thgen/command-line-rest rest)  ; rest: (DEVICE [SETTING...])
  (let* ((device (or (and (pair? rest)
                          (string? (car rest))
                          (string->symbol (car rest)))
                     (error "no device specified")))
         (parsed (getopt-long
                  rest (map (lambda (config)
                              (list (C:name config) '(value #t)))
                            (apply append (SCRATCH #:vars device))))))
    (thgen device (delq (assq '() parsed) parsed))))

;;; (thud thgen) ends here
