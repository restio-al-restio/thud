;;; (thud io) --- I/O and translation

;; Copyright (C) 1998-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file has code for translating TH into other HDLs.
;; The convention is to export `display-blk-OTHERHDL'.
;; There is also dump support.

;;; Code:

(define-module (thud io)
  #:export (translate                   ; translations
            display-translation-formats
            make-vcd-mangler)           ; dump support
  #:use-module ((srfi srfi-1) #:select (lset-difference))
  #:use-module ((thud config-info) #:select (%thud-config-info))
  #:use-module ((thud types) #:select (make-dic
                                       d-set! d-ref d-size d->list
                                       vn-bw
                                       enof-bw enof-rep
                                       blk-ref?
                                       blk-ports blk-nets
                                       blk-flops blk-irefs
                                       iref-maps iref-blk
                                       instance-blk
                                       in-port? PORT? net? flop?))
  #:use-module ((thud state) #:select (k session))
  #:use-module ((thud com) #:select (dbg fln n->fw-bstr)))

;;;---------------------------------------------------------------------------
;;; Translation support
;;;

(define op-translate #f)
(define (use-op-translate-proc! proc) (set! op-translate proc))

(define cur-sig #f)                     ; ugh
(define support-wires #f)               ; each element: (WHENCE TO SYM . ETC)

;; TODO TODO TODO: Handle new argument semantics for `*bofh*'.
;; See runtime.scm for more info.
;;
(define (display-bofh-infix pre-post-sep len data)
  (define (find-whence whence) (assoc whence support-wires))
  (display (list-ref pre-post-sep 0))
  (let loop ((ls data))
    (and (pair? ls)
         (begin
           (let ((datum (car ls)))
             (if (pair? (car datum))
                 (let ((already (find-whence (car datum))))
                   (if already
                       (display (caddr already))
                       (let ((new (list (car datum) '() (gensym "__sup"))))
                         (set! support-wires (cons new support-wires))
                         (display (caddr new)))))
                 (display (car datum))))
           (and (pair? (cdr ls)) (display (list-ref pre-post-sep 2)))
           (force-output)
           (loop (cdr ls)))))
  (display (list-ref pre-post-sep 1)))

(define (display-bofh-infix a b c)      ; reminder
  (error "*FIX:display-bofh-infix*"))

(define (display-infix exp)
  (cond ((not (pair? exp)) (display exp))
        ((eq? (car exp) '*bofh*) (display-bofh-infix
                                  (car (op-translate '*bofh*)) ; hmm
                                  (cadr exp) ; hmm
                                  (cddr exp))) ; hmm
        ((eq? (car exp) 'bx)
         (display-infix (cadr exp))
         (display "[") (display (caddr exp)) (display "]"))
        ((eq? (car exp) 'Bx)
         (display-infix (cadr exp))
         (let ((start (caddr exp)) (len (cadddr exp)))
           (display "[") (display (+ start len)) (display ":")
           (display start) (display "]")))
        (else
         (letrec ((op (op-translate (car exp)))
                  (first (cadr exp)))
           (display "(")
           (display-infix first)
           (map (lambda (x)
                  (display " ")
                  (display op)
                  (display " ")
                  (display-infix x))
                (cddr exp))
           (display ")")))))

(define (infix exp)                     ; yuk
  (with-output-to-string (lambda () (display-infix exp))))

;;;---------------------------------------------------------------------------
;;; Verilog translation
;;;

(define verilog-ops-map (make-dic))
(for-each (lambda (mapping)
            (d-set! verilog-ops-map (car mapping) (cdr mapping)))
          '((ash                        <<)
            (=                          ==)
            (*bofh*                     ("{" "}" ","))
            (*one-liner-comment-start*  "// ")
            ;; Add new mappings here.
            ))

(define (op-translate-verilog op)
  (or (d-ref verilog-ops-map op)
      op))

(define (display-blk-verilog nm-blk)
  (let ((name (car nm-blk))
        (blk  (cdr nm-blk)))
    (define (display-bw enof)
      (or (= 1 (enof-bw enof))
          (begin
            (display "[") (display (1- (enof-bw enof)))
            (display ":0]")))
      (display "\t"))
    (define (display-name-semi name)
      (fln "~A;" name))
    (define (display-declare prefix name enof)
      (display prefix) (display "\t")
      (display-bw enof)
      (display-name-semi name))
    (define (display-assign name rep)
      (if (or (list? rep) (not (pair? rep)))
          (begin
            (let ((mark support-wires))
              (set! cur-sig name)
              (fln "assign ~S = ~A;" name (infix rep))
              (let loop ((walker support-wires))
                (and (not (eq? walker mark))
                     (begin
                       (set-car! (cdar walker) cur-sig) ; ugh
                       (loop (cdr walker)))))
              ))))

    ;; Do it!
    ;;
    (let ((have-ports (and (blk-ports blk) (< 0 (d-size (blk-ports blk)))))
          (have-nets  (and (blk-nets  blk) (< 0 (d-size (blk-nets  blk))))))

      (use-op-translate-proc! op-translate-verilog)
      (set! support-wires '())

      (fln "//-------------------------------------------------------------")
      (fln "// ~S" name)
      (fln "//")
      (fln "module ~S" name)

      (if have-ports
          (let ((count (d-size (blk-ports blk))))
            (fln " (")
            (for-each (lambda (nm-port)
                        (let ((name (car nm-port)))
                          (fln "\t~S~A" name (if (< 1 count) "," ""))
                          (set! count (1- count))))
                      (d->list (blk-ports blk)))
            (fln " )"))
          (fln "// no ports"))
      (fln ";")

      (and have-ports
           (begin
             (fln "\n// Port declarations")
             (for-each (lambda (nm-port)
                         (let ((name (car nm-port))
                               (port (cdr nm-port)))
                           (display-declare (if (in-port? port) "input" "output")
                                            name port)))
                       (d->list (blk-ports blk)))))

      (and have-nets
           (begin
             (fln "\n// Wire declarations")
             (for-each (lambda (nm-net)
                         (let ((name (car nm-net))
                               (net  (cdr nm-net)))
                           (display-declare "wire" name net)))
                       (d->list (blk-nets blk)))))

      (and have-ports
           (begin
             (fln "\n// Port assignments")
             (for-each (lambda (nm-port)
                         (let* ((name (car nm-port))
                                (port (cdr nm-port))
                                (rep  (enof-rep port)))
                           (or (in-port? port)
                               (display-assign name rep))))
                       (d->list (blk-ports blk)))))

      (and have-nets
           (begin
             (fln "\n// Wire assignments")
             (for-each (lambda (nm-net)
                         (let* ((name (car nm-net))
                                (net  (cdr nm-net))
                                (rep  (enof-rep net)))
                           (if (or (list? rep) (not (pair? rep)))
                               (fln "assign ~S = ~A;" name (infix rep)))))
                       (d->list (blk-nets blk)))))

      (and (blk-flops blk) (< 0 (d-size (blk-flops blk)))
           (begin
             (fln "\n// Flop wires")
             (for-each (lambda (nm-flop)
                         (let ((name (car nm-flop))
                               (flop (cdr nm-flop)))
                           (display-declare "wire" name flop)))
                       (d->list (blk-flops blk)))

             (fln "\n// Flops")
             (for-each (lambda (nm-flop)
                         (let ((name (car nm-flop))
                               (flop (cdr nm-flop)))
                           (fln "thud_flop out_~S ( .X (~S), .A (~A));"
                                name name (infix (enof-rep flop)))))
                       (d->list (blk-flops blk)))))

      (and (blk-irefs blk) (< 0 (d-size (blk-irefs blk)))
           (let ((find-sup (lambda (n s) (assoc (cons n s) support-wires))))
             (fln "\n// Instantiations")
             (for-each (lambda (nm-iref)
                         (let* ((name (car nm-iref))
                                (iref (cdr nm-iref))
                                (count (d-size (iref-maps iref))))
                           (fln "~S ~S ("
                                (let ((blk (iref-blk iref)))
                                  (let loop ((ls (session list #:blocks)))
                                    (and ls
                                         (if (eq? blk (cdar ls))
                                             (caar ls)
                                             (loop (cdr ls))))))
                                name)
                           (for-each (lambda (irp-bp)
                                       (let ((irp (car irp-bp))
                                             (bp  (cdr irp-bp)))
                                         (display " .") (display irp) (display "\t")
                                         (display "(")
                                         (let ((sup #f))
                                           (cond ((and (list? bp)
                                                       (or (eq? (car bp) 'bx)
                                                           (eq? (car bp) 'Bx))
                                                       (begin
                                                         (set! sup (find-sup name irp))
                                                         sup)
                                                       (eq? (cadr sup) (cadr bp)))
                                                  (display (caddr sup)))
                                                 (else
                                                  (display-infix bp))))
                                         (fln ")~A" (if (< 1 count) "," ""))
                                         (set! count (1- count))))
                                     (d->list (iref-maps iref)))
                           (fln ");")))
                       (d->list (blk-irefs blk)))))

      (and (> (length support-wires) 0)
           (begin
             (fln "\n// Support wires")
             (for-each (lambda (sup)
                         (fln "wire ~S;" (caddr sup)))
                       support-wires)))

      (fln "\nendmodule // ~S\n" name))))

;;;---------------------------------------------------------------------------
;;; Translation in general

(define translation-formats
  (list (cons 'verilog display-blk-verilog)
        ;; Add new translations here.
        ))

(define (translate format file . blk-names)
  "Using FORMAT, save to FILE each of the translated BLK-NAMES.
Use `display-translation-formats' to see what is available."
  (let ((displayer (assq format translation-formats)))
    (and displayer
         (with-output-to-file file
           (lambda ()
             (for-each (cdr displayer)
                       (map (lambda (name)
                              (cons name (session #:blocks name)))
                            blk-names)))))))

(define (display-translation-formats)
  "Display a list of supported translation formats."
  (map car translation-formats))

;;;---------------------------------------------------------------------------
;;; Dump support
;;;

(define *glyphs* (string-append
                  "!\"#$%&'()*+-./"
                  "0123456789"
                  ":;<=>?@"
                  "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                  "[\\]^_`"
                  "abcdefghijklmnopqrstuvwxyz"
                  "{|}~"))

(define *glen* (string-length *glyphs*))

(define (glyph-generator)
  (let ((base #f)
        (b-ls #f)
        (nidx #f))

    (define (new-base! bls)
      (set! base
            (apply string-append
                   (reverse! (map string
                                  (map (lambda (x)
                                         (string-ref *glyphs* x))
                                       bls)))))
      (set! b-ls bls)
      (set! nidx 0))

    ;; do it!
    (new-base! (list 0))
    (lambda ()
      (let ((rv (string-append base (substring *glyphs* nidx (1+ nidx)))))
        (set! nidx (1+ nidx))
        (and (= *glen* nidx)
             (new-base! (let loop ((bls b-ls))
                          (if (null? bls)
                              (list 0)
                              (let ((lsb-char-idx (1+ (car bls))))
                                (if (not (= *glen* lsb-char-idx))
                                    (cons lsb-char-idx (cdr bls))
                                    (cons 0 (loop (cdr bls)))))))))
        rv))))

(define /-char (string-ref (k #:inm-term) 0))
(define /-sym (k #:inm-term-sym))

(define (make-vcd-mangler deref cc)
  (let ((all? #f)                       ; all nodes in all instances?
        (vars #f)                       ; specific nodes
        (port #f)                       ; output (scheme) port
        (gmap #f)                       ; hash vn to glyph
        (prev #f)                       ; hash vn to previous values
        (newg #f))                      ; proc to generate glyphs

    ;; Register symbols in LS to be dumped.
    ;; LS may also be a list of one list of symbols (blech).
    ;;
    (define (register ls)
      (dbg DBGDMP (fln "register: ~S" ls))
      (cond
       ((null? ls)       (set! all? #t))
       ((pair? (car ls)) (register (car ls)))
       (else (set! all? #f)
             (for-each (lambda (var)
                         (dbg DBGDMP (fln "[-d] dumping var ~S" var))
                         (set! vars (cons var vars)))
                       ls))))

    ;; Walk `(session #:instances)' to map vars to glyphs.
    ;; Stash the mapping and display it, too.
    ;;
    (define (get/display-new-dump-mapping)

      (define (mark-and-spew name full enof)
        (let ((g (newg)))
          (d-set! gmap full g)
          (fln "$var ~A\t ~S ~A \t ~S $end"
               (cond ((PORT? enof) 'wire)
                     ((net?  enof) 'wire)
                     ((flop? enof) 'reg)
                     (else         'hmm))
               (enof-bw enof) g name)))

      (define (process-instance instance-name instance)

        (define (process-enof name-enof)
          (let* ((name (car name-enof))
                 (full (symbol-append instance-name name)))
            (if vars
                ;; explicit request
                (and (memq full vars)
                     (mark-and-spew name full (cdr name-enof)))
                ;; everybody
                (mark-and-spew name full (cdr name-enof)))))

        (define (process-iref name-iref)
          (let* ((iref-name (car name-iref))
                 (fullname  (symbol-append instance-name iref-name /-sym)))
            (process-instance fullname (session #:instances fullname))))

        (fln "\n$scope module ~A $end"
             (let* ((name (symbol->string instance-name))
                    (m1 (1- (string-length name))))
               (substring name (1+ (string-rindex name /-char 0 m1)) m1)))
        (let ((blk (instance-blk instance)))
          (for-each process-enof (d->list (blk-ports blk)))
          (for-each process-enof (d->list (blk-nets  blk)))
          (for-each process-enof (d->list (blk-flops blk)))
          (for-each process-iref (d->list (blk-irefs blk))))
        (fln "$upscope $end"))

      ;; Do it!
      (and (null? vars)                 ; simplify
           (set! vars #f))

      (for-each (lambda (name-instance)
                  (let ((instance-name (car name-instance))
                        (instance      (cdr name-instance)))
                    (or (blk-ref? (instance-blk instance))
                        (process-instance instance-name instance))))
                (session list #:instances))
      (fln "$enddefinitions $end"))

    ;; If necessary, init `port', write out header and initial dump.
    ;;
    (define (prep)
      (dbg DBGDMP (fln "[-d] dumping prep..."))
      (and all?
           (set! vars (map car (session list #:vob)))
           (dbg DBGDMP (fln "[-d] dumping everything")))
      ;; remove aliases
      (set! vars (lset-difference eq? vars (map car (session list #:aliases))))
      (for-each (lambda (vn)
                  (d-set! prev vn *unspecified*))
                vars)
      (dbg DBGDMP
           (fln "[-d] dumping ~A vars:  ~S" (length vars) vars))
      (and=> (and (not (null? vars))
                  (session #:vcd-file))
             (lambda (filename)
               (or (dbg DBGDMP (fln "[-d] Going to dump to ~S" filename)) #t)
               (set! port (open-output-file filename))
               (with-output-to-port port
                 (lambda ()
                   (fln "~A\n$version\n    thud ~A\n$end"
                        (strftime "$date\n    %b %d, %Y  %H:%M:%S\n$end"
                                  (localtime (current-time)))
                        (%thud-config-info 'VERSION))
                   (fln "$timescale\n     1ns\n$end")
                   (get/display-new-dump-mapping)
                   (display "#0\n")
                   (display "$dumpvars\n")
                   (dump-vars!)
                   (display "$end\n")))
               (add-hook! (session #:aft-cycle) do-dump!)
               #t)))

    ;; Simulation time: do actual dumping.
    ;;
    (define (do-dump!)
      (display #\# port)
      (display (1+ (cc)) port)
      (newline port)
      (dump-vars!))

    (define (dump-vars!)
      (define (dump-vn! vn val)
        (let ((bw (vn-bw vn)))
          (cond ((not (integer? val)) (display #\x port))
                ((or (= 1 bw) (negative? val)) (display val port))
                (else (display #\b port)
                      (display (n->fw-bstr bw val) port)
                      (display #\space port))))
        (display (d-ref gmap vn) port)
        (newline port))
      (for-each (lambda (vn)
                  (let ((val (deref vn)))
                    (cond ((eq? val (d-ref prev vn)))
                          (else (d-set! prev vn val)
                                (dump-vn! vn val)))))
                vars))

    (define (reset!)
      (remove-hook! (session #:aft-cycle) do-dump!)
      (and port (close-port port))
      (set! port #f)
      (set! all? #f)
      (set! vars '())
      (set! gmap (make-dic))
      (set! prev (make-dic))
      (set! newg (glyph-generator)))

    ;; reset and return closure
    (reset!)
    (lambda (command . args)
      (apply (case command
               ((#:reset!) reset!)
               ((#:count) (lambda ignored ; for "make check"
                            (if vars (length vars) 0)))
               ((#:register!) register)
               ((#:prep!) prep))
             args))))

;;; (thud io) ends here
