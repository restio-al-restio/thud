;;; (thud com) --- Common types, state and procedures

;; Copyright (C) 1998-2001, 2005, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file defines common procedures.

;;; Code:

(define-module (thud com)
  #:export (fs                          ; boot-type stuff
            fln
            dbg
            thud-load-from-path

            die maybe-say assert        ; low-level support
            n->fw-bstr top-level-name?

            t2 t5                       ; formatting
            human-readable-time-stamp

            thud-execute-user-script    ; executing user scripts

            vn-var vn-val)
  #:use-module ((srfi srfi-13) #:select (string-join
                                         substring/shared))
  #:use-module ((thud state) #:select (k session)))             ; vob surfing

;;;---------------------------------------------------------------------------
;;; Boot-type stuff that must go first

(define (fs s . args)
  (apply simple-format #f s args))

(define (fln s . args)
  (apply simple-format #t s args)
  (newline))

(define-macro (dbg <flag> . <body>)
  `(or (zero? (logand (,k ',<flag>) (,session #:opt-debug)))
       (begin
         ,@<body>)))

;;;---------------------------------------------------------------------------
;;; Low-level support procedures

;; Return LIST as a string w/ SEP between elements.
;; Non-string LIST elements are passed through `object->string'.
;;
(define (list->sep-string list sep)
  (string-join (map (lambda (obj)
                      (or (and (string? obj) obj)
                          (object->string obj)))
                    list)
               sep))

;; Die.
;;
(define (die . args)
  "Call `scm-error' with key `thud-error', using ARGS as a message."
  (scm-error 'thud-error #f (list->sep-string args " ") '() #f))

(define (maybe-say msg-list)
  (or (null? msg-list)
      (fln "~A" (car msg-list))))

;; Throws `thud-assert' if <exp> is #f.
;;
(define-macro (assert <exp>)
  `(or ,<exp>
       (begin
         (,fln "wff?! not true: ~S" ',<exp>)
         (throw 'thud-assert))))

;; Given a BIT-WIDTH, format NUMBER to a fixed-width binary string.
;; There is no check if NUMBER takes more than BIT-WIDTH bits.
;; NUMBER may be negative.  If NUMBER is #t, it is taken as 1.
;; If NUMBER is #f, return a string of "x" characters.
;;
(define n->fw-bstr
  (let* ((top 129)
         (range (iota top))
         (all0 (make-string top #\0))
         (allx (make-string top #\x))
         (mapv (lambda (proc) (list->vector (map proc range))))
         (vec0 (mapv (lambda (bw) (substring/shared all0 (- top bw)))))
         (vecx (mapv (lambda (bw) (substring/shared allx (- top bw)))))
         (vecn (mapv (lambda (n) (number->string n 2)))))
    (lambda (bw n)
      (assert (< 0 bw))
      (if (not n)
          (if (< bw top)
              (vector-ref vecx bw)
              (make-string bw #\x))
          (let* ((nstr (cond ((eq? n #t) "1")
                             ((< -1 n) (if (< n top)
                                           (vector-ref vecn n)
                                           (number->string n 2)))
                             (else (number->string (+ (ash 1 bw) n) 2))))
                 (fill (max 0 (- bw (string-length nstr)))))
            (string-append (if (< bw top)
                               (vector-ref vec0 fill)
                               (make-string fill #\0))
                           nstr))))))


;; Return non-#f if S is a top-level name.
;;
(define top-level-name?
  (let ((rx (k #:top-level-name-re)))
    (lambda (s) (regexp-exec rx (symbol->string s)))))

;; Primitive formatting.
;;
(define (t2 n) (/ (floor (*   100 n))   100))
(define (t5 n) (/ (floor (* 10000 n)) 10000))

;; Return time formatted for human reading.  Optional arg
;; @var{time-vector} specifies another time rather than the result
;; of @code{gettimeofday}.
;;
(define (human-readable-time-stamp . time-vector)
  (strftime "%Y%m%d %a %H:%M:%S"
            (if (null? time-vector)
                (localtime (car (gettimeofday)))
                (car time-vector))))

;;;---------------------------------------------------------------------------
;;; Executing user scripts

(define (thud-execute-user-script file)
  "Read and evaluate each form from driver FILE.
Save the \"current module\" beforehand and restore it afterwards."
  (dbg DBGUSR (fln "[-d] Starting user script: ~S" file))
  (call-with-input-file file
    (lambda (port)
      (let loop ((form (read port)))
        (or (eof-object? form)
            (begin
              (dbg DBGUSR
                   (fln "[-d] user script form: ~S"
                        (if (list? form)
                            `(,(car form)
                              ,@(if (memq (car form)
                                          '(define
                                             defmacro
                                             define-module))
                                    `(,(cadr form) ...)
                                    '(...)))
                            form)))
              ((if (defined? 'primitive-eval)
                   primitive-eval
                   eval)
               form)
              (loop (read port))))))))

;;;---------------------------------------------------------------------------
;;; Values surfing

(define (vn-var vn) (session #:vob vn))
(define (vn-val vn) (variable-ref (vn-var vn)))

;;; (thud com) ends here
