;;; v2th.el --- Translate verilog to thud hdl.

;; Copyright (C) 1998-2001, 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of THUD.
;;
;; THUD is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; THUD is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with THUD.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This is a largely-unrewarding execerise in recursive descent.  Does not
;; handle any behavioral constructs.  Handles other simple constructs OK.
;; Should be re-implemented using proper lexer/parser.

;;; Code:

;; This file exports one function: v2th.  The argument is a list of files to
;; be translated.  For each verilog file foo.v, it produces foo.th in the same
;; directory.

(defun hex-string-to-int (s)
  "Converts hexadecimal string to an integer.
e.g.: (hex-string-to-int \"0x35\") => 53"
  (interactive "sHex Number: ")
  (let ((s (copy-sequence s))
        (n (string-match "0[xX]" s))
        (acc 0))
    (when n (setq s (substring s (+ n 2))))
    (catch 'done-parsing
      (while t
        (setq acc (+ (* 16 acc)
                     (let ((c (string-to-char s)))
                       (cond ((and (>= c ?0) (<= c ?9))
                              (- c ?0))
                             ((and (>= c ?a) (<= c ?f))
                              (+ 10 (- c ?a)))
                             ((and (>= c ?A) (<= c ?F))
                              (+ 10 (- c ?A)))
                             (t
                              (throw 'done-parsing acc)))))) ; throw!
        (setq s (substring s 1))))
    (if (interactive-p)
        (message (int-to-string acc))
      acc)))

(defun bin-string-to-int (s)
  "Converts binary string to an integer.
e.g.: (bin-string-to-int \"011011\") => 27"
  (interactive "sHex Number: ")
  (let ((s (copy-sequence s))
        (acc 0))
    (catch 'done-parsing
      (while t
        (setq acc (+ (* 2 acc)
                     (let ((c (string-to-char s)))
                       (cond ((= ?0 c) 0)
                             ((= ?1 c) 1)
                             (t (throw 'done-parsing acc)))))) ; throw!
        (setq s (substring s 1))))
    (if (interactive-p)
        (message (int-to-string acc))
      acc)))

(defun go-pm () (goto-char (point-min)))

(defvar curly-brace-count 0)
(defvar curly-brace-expansion '())

(defun abbreviate-curly-brace-exp ()
  (setq curly-brace-count 1)
  (setq curly-brace-expansion '())
  (go-pm)
  (while (re-search-forward "{\\(\n\\|[^}]\\)+}" (point-max) t)
    (let* ((nstr (number-to-string curly-brace-count))
           (instead (concat "<<" nstr ">>")))
      (push (cons nstr (substring (match-string 0) 1 -1))
            curly-brace-expansion)
      (setq curly-brace-count (1+ curly-brace-count))
      (replace-match instead))))

(defun t1 (str)
  (let (ret)
    (while (or (string-match "^\\(!\\)\\(.*\\)" str)
               (string-match "^\\(\(\\)\\(.*\\)" str))
      (push (match-string 1 str) ret)
      (setq str (match-string 2 str)))
    (push str ret)
    (reverse ret)))

(defun t2 (str)
  (let (ret)
    (let (ret)
      (while (string-match "\\(.*\\)\\(\)\\)$" str)
        (push (match-string 2 str) ret)
        (setq str (match-string 1 str)))
      (push str ret)
      ret)))

(defun t3 (str)
  (let ((num-re
         "\\([0-9]+\\)*\\s-*'\\(h[0-9a-fA-F]+\\|b[01]+\\|[0-9]+\\)")
        start)
    (while (setq start (string-match num-re str))
      (let* ((val-str (match-string 2 str))
             (first-c (aref val-str 0))
             (len     (length (match-string 0 str)))
             xlat)
        (cond ((or (= ?h first-c) (= ?H first-c))
               (setq xlat (hex-string-to-int (substring val-str 1))))
              ((or (= ?b first-c) (= ?B first-c))
               (setq xlat (bin-string-to-int (substring val-str 1))))
              (t
               (setq xlat (string-to-number val-str))))
        (setq str (concat (substring str 0 start)
                          (number-to-string xlat)
                          (substring str (+ start len)))))))
  (car (read-from-string
        (concat "( "
                (mapconcat #'identity
                           (apply #'append
                                  (mapcar #'t2
                                          (apply #'append
                                                 (mapcar #'t1
                                                         (split-string str)))))
                           " ")
                " )"))))

(defun t4 (form)
  (if (atom form)
      form
    (let ((walker form) res)
      (while walker
        (if (and (car walker) (cdr walker)
                 (arrayp (cadr walker)))
            (let ((extract-spec (aref (cadr walker) 0))
                  begin dur)
              (cond ((symbolp extract-spec)
                     (let ((spec-str (symbol-name extract-spec)))
                       (or (string-match "\\([0-9]+\\):\\([0-9]+\\)" spec-str)
                           (error "cannot do bx for: %s" spec-str))
                       (setq begin (string-to-number
                                    (match-string 2 spec-str)))
                       (setq dur (- (string-to-number
                                     (match-string 1 spec-str))
                                    begin))))
                    ((numberp extract-spec)
                     (setq begin extract-spec)
                     (setq dur 1)))
              (push (if (= 1 dur)
                        (list 'bx begin (t4 (car walker)))
                      (list 'Bx begin dur (t4 (car walker))))
                    res)
              (setq walker (cdr walker)))
          (push (t4 (car walker)) res))
        (setq walker (cdr walker)))
      (reverse res))))

(defun t5 (form)
  (cond ((atom form) form)
        ((and (listp form) (= 2 (length form)) (eq '! (car form)))
         (list '! (t5 (cdr form))))
        (t
         (let (walker res)

           (setq walker form)
           (while walker
             (if (eq '! (car walker))
                 (progn
                   (push (list '! (t5 (cadr walker))) res)
                   (setq walker (cddr walker)))
               (push (t5 (car walker)) res)
               (setq walker (cdr walker))))

           (reverse res)))))

(defun t6 (form)
  (cond ((numberp form)
         form)
        ((atom form)
         (let* ((string-rep (symbol-name form)))
           (if (string-match "^<<\\([0-9]+\\)>>" string-rep)
               (let* ((nstr (match-string 1 string-rep))
                      (exp-str-list (split-string
                                     (cdr (assoc* nstr
                                                  curly-brace-expansion
                                                  :test #'string=))
                                     ",")))
                 (cons 'c. (mapcar #'translate-str-exp exp-str-list)))
             form)))
        ((and (listp form) (= 2 (length form)) (eq '! (car form)))
         (list '! (t6 (cdr form))))
        ((and (listp form) (= 3 (length form)) (eq 'bx (car form)))
         (list 'bx (nth 1 form) (t6 (caddr form))))
        ((and (listp form) (= 4 (length form)) (eq 'Bx (car form)))
         (list 'Bx (nth 1 form) (nth 2 form) (t6 (cadddr form))))
        ((and (listp form) (= 3 (length form)) (memq (cadr form) '(== ===)))
         (list (cadr form) (t6 (car form)) (t6 (caddr form))))
        (t
         (let ((res (list '|)))
           (catch 'done
             (while form
               (push (t6 (car form)) res)
               (cond ((null (cdr form))
                      (throw 'done (reverse res)))
                     ((eq '| (cadr form))
                      (setq form (cddr form)))
                     ((memq (cadr form) '(& ^))
                      (pop res)
                      (let* ((and-res (list (cadr form))))
                        (while (memq (cadr form) '(& ^))
                          (push (t6 (car form)) and-res)
                          (setq form (cddr form)))
                        (push (t6 (car form)) and-res)
                        (push (reverse and-res) res)
                        (setq form (cddr form))))))
             (reverse res))))))

(defun t7 (form)
  (cond ((atom form) form)
        ((and (listp form) (= 2 (length form)) (eq '! (car form)))
         (list '! (t7 (cadr form))))
        ((= 1 (length form))
         (t7 (car form)))
        ((and (memq (car form) '(^ | &)) (= 2 (length form)))
         (t7 (cadr form)))
        (t
         (cons (car form)
               (mapcar #'t7 (cdr form))))))

(defun translate-str-exp (str)
  (save-match-data
    (t7 (t6 (t5 (t4 (t3 str)))))))

(defun v2th-file (file)
  "Translate FILE.v to FILE.th.  Currently, comments ruthlessly eliminated."
  (interactive "fFile: ")
  (let ((th-file (concat (progn
                           (string-match "\\(.*\\)\\.v$"  file)
                           (match-string 1 file))
                         ".th")))
    (message "Translating %s to %s." file th-file)
    (if (get-buffer "*v2th-file*")
        (kill-buffer (get-buffer "*v2th-file*")))
    (switch-to-buffer "*v2th-file*")
    (insert-file-contents file)

    (message "Deleting comments...")
    (go-pm)
    (while (re-search-forward "\\s-*//.*$" (point-max) t)
      (replace-match ""))
    (go-pm)
    (while (search-forward "/*" (point-max) t)
      (let ((p (- (point) 2)))
        (search-forward "*/")
        (delete-region p (point))))

    (message "Handling blk...")
    (go-pm)
    (while (search-forward "endmodule" (point-max) t)
      (replace-match "\n;;; end blk\n"))
    (go-pm)
    (while (search-forward "module" (point-max) t)
      (replace-match "(blk")
      (forward-sexp)
      (let ((p (point)))
        (search-forward ";")
        (delete-region p (point))
        (insert "\n)")))

    (message "Handling declarations...")
    (let ((count 0))                    ; Handling declarations
      (go-pm)
      (while (re-search-forward
              (concat
               "^\\s-*\\(input\\|output\\|wire\\|reg\\)\\s-*"
               "\\(\\[\\([0-9]+\\):\\([0-9]+\\)\\]\\)*\\s-*"
               "\\([^;]*\\(\n[^;]*\\)*\\);")
              (point-max) t)
        (message "Handling declarations... %d" (setq count (1+ count)))
        (let ((type   (cond ((string= "input"  (match-string 1)) "in")
                            ((string= "output" (match-string 1)) "out")
                            (t (match-string 1))))
              (width  (or (and (match-string 2)
                               (number-to-string
                                (- (string-to-number (match-string 3))
                                   (string-to-number (match-string 4))
                                   -1)))
                          ""))
              (things (save-match-data
                        (split-string (match-string 5) "\\s-*,\\s-*"))))
          (replace-match "")
          (dolist (thing things)
            (insert "(" type "\t" width "\t" thing ")\n")))))

    (let ((msg "Translating expressions... %d")
          (count 0))
      (message "Curly brace kludge...")
      (abbreviate-curly-brace-exp)      ; expanded by t6
      (go-pm)
      (while (re-search-forward "assign[^;]+\\(\n[^;]+\\)*;" (point-max) t)
        (message msg (setq count (1+ count)))
        (delete-char -1)
        (insert ")")
        (save-excursion
          (goto-char (match-beginning 0))
          (kill-word 1)
          (insert "(a.")))
      (go-pm)
      (while (re-search-forward
              "(\\(a\\.\\|wire\\)\\s-+\\(\\S-+\\)\\s-*=" (point-max) t)
        (message msg (setq count (1+ count)))
        (let* ((b (point))
               (e (save-excursion
                    (goto-char (match-beginning 0))
                    (forward-list 1)
                    (1- (point))))
               (exp (translate-str-exp (buffer-substring b e))))
          (delete-region (1- b) e)
          (goto-char (1- b))
          (pp exp (current-buffer)))))

    (let ((msg "Converting instantiations... %d")
          (count 0))
      (go-pm)
      (while
          (re-search-forward
           "^\\([a-zA-Z_][a-zA-Z_0-9]+\\s-+[a-zA-Z_][a-zA-Z_0-9]+\\s-*\\)("
           (point-max) t)
        (message msg (setq count (1+ count)))
        (replace-match "(w/ \\1\n")
        (let* ((b (point))
               (e (progn (search-forward ";") (point)))
               (all-gone (buffer-substring b (progn (search-backward ")")
                                                    (point))))
               (map-strings (split-string all-gone ",")))
          (delete-region b e)
          (dolist (str map-strings)
            (let ((named (save-match-data (string-match "\\." str)))
                  name-str exp-str exp)
              (if named
                  (let ((named-end (save-match-data (string-match "(" str))))
                    (setq name-str (substring str (1+ named) named-end))
                    (setq exp-str  (substring str (1+ named-end))))
                (setq exp-str str))
              (setq exp (translate-str-exp exp-str))
              (insert (format "    (%s%s)\n"
                              (if named (concat name-str " . ") "? . ")
                              (pp exp)))
              )
            )
          )
      (insert " )\n")))

    (message "Moving ports...")
    (go-pm)
    (while (re-search-forward "(blk.*\n" (point-max) t)
      (let ((here (point))
            (pmax (progn (search-forward ";;; end blk") (point))))
        (goto-char here)
        (when (get-buffer "*v2th-aux*")
          (kill-buffer "*v2th-aux*"))
        (generate-new-buffer "*v2th-aux*")
        ;; regexp anchors to bol to avoid aliasing w/ instance portnames
        (while (re-search-forward "^(\\(in\\|out\\).*)\n" pmax t)
          (append-to-buffer "*v2th-aux*" (match-beginning 0) (match-end 0))
          (replace-match ""))
        (goto-char here)
        (insert-buffer "*v2th-aux*")
        (goto-char pmax)))

    (let ((msg "Cleaning up... %d")
          (count 6))
      (message msg (setq count (1- count)))
      (go-pm)
      (while (search-forward "c\\." (point-max) t)
        (forward-char -1)
        (delete-char -1))
      (message msg (setq count (1- count)))
      (go-pm)
      (while (re-search-forward "!\n\\s-+(" (point-max) t)
        (replace-match "! ("))
      (message msg (setq count (1- count)))
      (go-pm)
      (while (re-search-forward "\\s-*\n\\s-*\n\\s-*\n" (point-max) t)
        (replace-match "\n\n"))
      (message msg (setq count (1- count)))
      (go-pm)
      (while (re-search-forward "(a.\\s-+" (point-max) t)
        (replace-match "(a. "))
      (message msg (setq count (1- count)))
      (goto-char (point-max))
      (while (re-search-backward "^(" 1 t)
        (indent-sexp)))

    (go-pm)
    (insert ";;; v2th-translated file, timestamp: "
            (current-time-string) "\n")
    (insert ";;; from: " file "\n")
    (insert ";;;   to: " th-file "\n")

    (write-file th-file nil)            ; don't ask for overwrite
    (message "Done.")
    ))

(defun v2th (files)
  "Translate FILES to thud hdl."
  (dolist (file files)
    (v2th-file file)))

;;; v2th.el ends here
