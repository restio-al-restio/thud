;;; thgen/characterize.scm
;;;
;;; Description: Characterize libraries.

;;; Code:

(use-modules (thud thgen) (ice-9 pretty-print))

(define (complexity obj)

  (define (count-cons obj)
    (cond ((eq? obj '()) 1)
          ((list? obj) (+ 1 (count-cons (car obj)) (count-cons (cdr obj))))
          ((vector? obj) (apply + (map count-cons (vector->list obj))))
          (else 0)))

  (define (count-atoms obj)
    (cond ((not (or (pair? obj) (list? obj))) 1)
          ((eq? obj '()) 0)
          ((list? obj)
           (+ (count-atoms (car obj))
              (if (not (cdr obj))
                  0
                  (count-atoms (cdr obj)))))
          ((vector? obj) (apply + (map count-atoms (vector->list obj))))))

  (let ((c (count-cons obj))
        (a (count-atoms obj)))
    (list c a (/ (* 1.0 c) a))))

;; Do it!
;;
(load-from-path "fully-decoded-mux.thgen-config")
(let ((spew (thgen 'fully-decoded-mux '((width . "42")
                                        (seq-beg . "13")))))

  (for-each (lambda (elem)
              (display ";;; ")
              (display (cadar elem))
              (display " -- ")
              (display (complexity elem)) (newline) (newline)
              (for-each (lambda (x) (pretty-print x) (newline)) elem))
            spew))

;;; thgen/characterize.scm ends here
