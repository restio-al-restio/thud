;;; compilation/compile-adder.scm
;;;
;;; Description: Compile adder and stimulus into a Scheme file.

;;; Commentary:

;; WARNING: The `(session #:cmax)' and `(session #:aft-cycle)' munging
;;          is not the final form of this functionality.

;;; Code:

(use-modules ((thud state) #:select (session))
             ((thud com) #:select (fln))
             ((thud svc0) #:select (add-only)))

(let ((source "../adder/adder.th")
      (compiled "adder.thc"))
  (add-only source "adder-stim.th")
  (set! (session #:cmax) 500)
  (add-hook! (session #:aft-cycle) display-scheduled-values)
  (fln "compiling ~A to ~A ..." source compiled)
  (compile-to compiled)
  (fln "compiling ... done")
  (system "cat adder.thc"))

(let ((cmd "THUD_DEBUG=1 guile -s adder.thc"))
  (fln "running ~A ..." cmd)
  (system cmd))

;;; compilation/compile-adder.scm ends here
