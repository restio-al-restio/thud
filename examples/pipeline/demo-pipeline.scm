;;; pipeline/demo-pipeline.scm
;;;
;;; Description: Demonstrate pipeline simulation (with dumping).

;;; Commentary:

;; First we define support procedures `dump-everything' and `display-stats'.
;; Next, the procedure `demo-pipline' displays system information, loads the
;; workspace with pipeline.th and pipeline-stim.th, turns on dumping for all
;; signals, simulates for 42 cycles and then displays stats.

;;; Code:

(use-modules ((thud svc0) #:select (display-system-info
                                    set-dump-file!
                                    dumpvars
                                    next
                                    add))
             ((thud evlog) #:select (display-event-log)))

(define (dump-everything filename)
  (set-dump-file! filename)
  (dumpvars))

(define (display-stats)
  (display-event-log "here is the event log:"))

(define (demo-pipeline)
  (display-system-info "hi!")
  (add "pipeline.th" "pipeline-stim.th")
  (dump-everything "pipeline-demo.dump")
  (next 42)
  (display-stats)
  (quit))

;; Do it!
(demo-pipeline)

;;; pipeline/demo-pipeline.scm ends here
