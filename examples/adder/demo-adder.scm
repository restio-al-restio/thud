;;; adder/demo-adder.scm
;;;
;;; Description: Demonstrate adders and their input permutations.

;;; Commentary:

;; This code corresponds with the example given in the THUD Manual, qv.
;; The procedure `demo-adder' loads the workspace with adder.th, permutes the
;; inputs, and finally displays the event log and execution form stats.

;;; Code:

(use-modules ((thud svc0) #:select (display-todo-list-reps
                                    add-only dumpvars))
             ((thud permute) #:select (permute))
             ((thud evlog) #:select (display-event-log)))

(define (demo-adder)
  (add-only "adder.th")
  (display-todo-list-reps "These are things a simulator must evaluate:")
  (dumpvars)
  (permute '/test/)
  (display-event-log "\nThese are the events:")
  (quit))

;; Do it!
(demo-adder)

;;; adder/demo-adder.scm ends here
